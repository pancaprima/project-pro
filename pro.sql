-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2015 at 09:41 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pro`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `display_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`, `display_name`) VALUES
('prima', '12345', 'Fernanda Panca Prima'),
('prima', '12345', 'Fernanda Panca Prima'),
('prima', '12345', 'Fernanda Panca Prima'),
('prima', '12345', 'Fernanda Panca Prima');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `image`, `created`, `updated`) VALUES
(1, 'Elektronik', 'elektronik.png', '2015-05-10 02:10:29', '2015-05-10 02:10:29'),
(2, 'Fashion', 'fashion.png', '2015-05-10 02:10:39', '2015-05-10 02:10:39'),
(3, 'Kuliner', 'kuliner.png', '2015-05-10 02:10:47', '2015-05-10 02:10:47');

-- --------------------------------------------------------

--
-- Table structure for table `log_product`
--

CREATE TABLE IF NOT EXISTS `log_product` (
  `id_product` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `ip_address` varchar(10) NOT NULL,
  `location` varchar(20) NOT NULL,
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_promo`
--

CREATE TABLE IF NOT EXISTS `log_promo` (
  `id_promo` varchar(255) NOT NULL,
  `time` datetime NOT NULL,
  `ip_address` varchar(10) NOT NULL,
  `location` varchar(20) NOT NULL,
  KEY `id_promo` (`id_promo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_store`
--

CREATE TABLE IF NOT EXISTS `log_store` (
  `id_store` int(5) NOT NULL,
  `time` datetime NOT NULL,
  `ip_address` varchar(10) NOT NULL,
  `location` varchar(20) NOT NULL,
  KEY `id_store` (`id_store`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_promo` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `promo_type` varchar(50) NOT NULL,
  `discount` int(3) NOT NULL,
  `normal_price` varchar(10) NOT NULL,
  `promo_factor` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_promo` (`id_promo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `id_promo`, `name`, `description`, `promo_type`, `discount`, `normal_price`, `promo_factor`, `image`, `tag`, `created`, `updated`) VALUES
(7, '1430923104kfc-kombo-duk-duk', 'KOMBO Duk-DUk', '<p>Rasakan kegurihan ayamnya</p>', 'Free Stuff', 69, '35000', '24000', 'product_kfc2.jpg', 'Ayam', '2015-05-06 17:34:23', '2015-05-06 17:34:23'),
(8, '1430928032valentine-promo', 'Converse All Star', '<p>?????????</p>', 'Buy n Get n', 50, '450000', '1,2', 'product_sport.JPG', 'Sepatu, Sporty, Casual', '2015-05-06 18:02:02', '2015-05-06 18:02:02'),
(9, 'PO_1430928985asc-promo', 'Komputer', '<p>blablablablablablabla</p>', 'Discount', 50, '770000', '50', 'pc.jpg', 'Pc, Murah, Komputer', '2015-05-06 18:23:43', '2015-05-06 18:23:43'),
(10, 'PO_1430928985asc-promo', 'Laptop', '<p>blablablablbla</p>', 'Discount', 50, '8000000', '50', 'laptop.jpg', 'Laptop, elektronik, murah', '2015-05-06 18:27:04', '2015-05-08 07:37:41'),
(12, '1430931103nike-sale-akhir-tahun', 'Nike SB', '<p>blablablabalbal</p>', 'Discount', 65, '1200000', '65', 'nike_sb.jpg', 'Sepatu, Sporty, Casual', '2015-05-06 18:52:29', '2015-05-08 07:37:19'),
(13, '1430931103nike-sale-akhir-tahun', 'Nike Running', '<p>blablablablablablabla</p>', 'New Price', 15, '650000', '550000', 'nike_run.jpg', 'Sepatu, Sporty, Casual', '2015-05-06 18:54:49', '2015-05-06 18:54:49'),
(14, '1431059866paket-hana', 'Sweet Ice Tea & Kimci Jeon', '<p>blablablabalbalabla</p>', 'New Price', 21, '38000', '30000', 'kimci_jeon.jpg', 'Kimci Jeon, Tea, Sweet', '2015-05-08 06:44:58', '2015-05-08 06:44:58'),
(15, '1431061609ice-cream', 'Strawberry Ice Cream', '<p>blabalbalbalbababa</p>', 'Buy n Get n', 50, '14000', '1,2', 'strawberry_ice_cream.jpg', 'Ice, Strawberry, Sweet', '2015-05-08 07:35:00', '2015-05-08 07:35:00'),
(16, '1431065398alfamart-promo-bulanan', 'Pepsodent', '<p>blablablablablablabbaba</p>', 'New Price', 22, '15300', '11900', 'pepsodent.jpg', 'Pasta Gigi, Gigi', '2015-05-08 13:11:20', '2015-05-08 13:11:20'),
(17, '1431065398alfamart-promo-bulanan', 'Tisu Wajah', '<p>blablablablablabalbalbala</p>', 'New Price', 33, '20900', '13900', 'tisu_wajah.jpg', 'tisu, wajah', '2015-05-08 13:13:54', '2015-05-08 13:13:54'),
(18, '1431065398alfamart-promo-bulanan', 'Minyak Goreng Mitra', '<p>blblablablablablabla</p>', 'New Price', 8, '23900', '21900', 'minyak_goreng_mitra.jpg', 'minyak goreng, mitra, masak', '2015-05-08 13:15:34', '2015-05-08 13:15:34'),
(19, '1431066266eiger-year-end-sale', 'Eiger Mountain Shoes', '<p>bablablablabalbalbla</p>', 'Discount', 50, '799999', '50', 'sepatu_eiger.jpg', 'Sepatu, Eiger, Adventure, Gunung', '2015-05-08 13:26:31', '2015-05-08 13:26:31'),
(20, '1431066266eiger-year-end-sale', 'Eiger Day Pack', '<p>blablablablabbal</p>', 'Discount', 50, '469000', '50', 'eiger_daypack.jpg', 'Tas, Mountain, Outdoor, Day Pack', '2015-05-08 13:28:24', '2015-05-08 13:28:24'),
(21, '1431066266eiger-year-end-sale', 'Sendal Gunung Eiger', '<p>blablablabalbalbalablablabla</p>', 'Discount', 50, '210000', '50', 'sendal.jpg', 'Sendal, Mountain, Outdoor', '2015-05-08 13:29:15', '2015-05-08 13:29:15'),
(22, '1431068058promo-ramadhan', 'Susu Gajah', '<p>blablablabalba</p>', 'Buy n Get n', 50, '12000', '1,2', 'KALIMILK__promo.jpg', 'Susu, Sapi, Minuman', '2015-05-08 13:57:32', '2015-05-08 13:57:32'),
(23, '1431069099fiesta-promo', 'McSpicy Fiesta', '<p>blablablablabalbala</p>', 'Free Stuff', 79, '17000', '13500', 'McSpicy_Paneer.png', 'Tea, Burger, Pedas', '2015-05-08 14:16:54', '2015-05-08 14:16:54'),
(24, '1431068058promo-ramadhan', 'Beer Bintang', '<p>....</p>', 'Discount', 25, '50000', '25', 'Bintang.jpg', 'beer,ramadhan', '2015-05-08 14:21:24', '2015-05-08 14:21:24');

-- --------------------------------------------------------

--
-- Table structure for table `promo`
--

CREATE TABLE IF NOT EXISTS `promo` (
  `id` varchar(255) NOT NULL,
  `id_store` int(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `level` int(3) NOT NULL,
  `promo_factor` varchar(255) NOT NULL,
  `promo_type` varchar(255) NOT NULL,
  `discount` float NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_store` (`id_store`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promo`
--

INSERT INTO `promo` (`id`, `id_store`, `title`, `level`, `promo_factor`, `promo_type`, `discount`, `start_date`, `end_date`, `description`, `image`, `slug`, `created`, `updated`) VALUES
('1430923104kfc-kombo-duk-duk', 1, 'KFC Kombo DUK DUK', 0, '24000', 'Free Stuff', 69, '2015-05-06 22:06:00', '2015-06-10 21:36:00', '<p>Dapatkan free jersey serta free pudding tiap pembeliannya guys</p>', 'PromoKFC.jpg', 'kfc-kombo-duk-duk', '2015-05-06 16:38:24', '2015-05-08 06:32:18'),
('1430928032valentine-promo', 2, 'Valentine Promo', 0, '1,2', 'Buy n Get n', 50, '2015-05-10 22:58:00', '2015-05-16 22:58:00', '<p>Datang ke store kami dan dapatkan diskon gede gedean yang kami hadirkan</p>', 'promo_sport.jpg', 'valentine-promo', '2015-05-06 18:00:32', '2015-05-06 18:00:32'),
('1430931103nike-sale-akhir-tahun', 4, 'Nike Sale Akhir Tahun', 0, '65', 'Discount', 65, '2015-05-17 23:50:00', '2015-05-30 23:50:00', '<p>blablablabalbalbalbala</p>', 'promo_nike.png', 'nike-sale-akhir-tahun', '2015-05-06 18:51:43', '2015-05-06 18:51:43'),
('1431059866paket-hana', 5, 'Paket Hana', 0, '30000', 'New Price', 21, '2015-05-20 11:36:00', '2015-07-31 11:36:00', '<p>Dapatkan kehematan dengan harga baru yang kami tawarkan</p>', 'promo.jpg', 'paket-hana', '2015-05-08 06:37:46', '2015-05-08 06:37:46'),
('1431061609ice-cream', 6, 'Ice Cream', 0, '1,2', 'Buy n Get n', 50, '2015-06-01 12:06:00', '2015-07-31 12:06:00', '<p>Dapatkan apa yang kamu mau</p>', 'promo_sushi.jpg', 'ice-cream', '2015-05-08 07:06:49', '2015-05-08 07:06:49'),
('1431065398alfamart-promo-bulanan', 7, 'Alfamart Promo Bulanan', 0, '13900', 'New Price', 33, '2015-05-10 13:09:00', '2015-05-31 13:09:00', '<p>blablablablabalbala</p>', 'promo_alfa.jpg', 'alfamart-promo-bulanan', '2015-05-08 13:09:58', '2015-05-08 13:09:58'),
('1431066266eiger-year-end-sale', 8, 'Eiger Year End Sale', 0, '50', 'Discount', 50, '2015-06-01 13:22:00', '2015-07-31 13:22:00', '<p>Dalam rangka memperingati tahun baru eiger adventure memberikan diskon akhir tahun besar besaran</p>', 'eiger_promo.jpg', 'eiger-year-end-sale', '2015-05-08 13:24:26', '2015-05-08 13:24:26'),
('1431068058promo-ramadhan', 9, 'Promo Ramadhan', 0, '1,2', 'Buy n Get n', 50, '2015-06-01 13:51:00', '2015-06-30 13:51:00', '<p>blablablablablabla</p>', 'kal.jpg', 'promo-ramadhan', '2015-05-08 13:54:18', '2015-05-08 13:54:18'),
('1431069099fiesta-promo', 10, 'Fiesta Promo', 0, '13900', 'Free Stuff', 79, '2015-06-06 14:07:00', '2015-07-04 14:08:00', '<p>blablablablablalbala</p>', 'promo_mcd.jpg', 'fiesta-promo', '2015-05-08 14:11:39', '2015-05-08 14:11:39'),
('1431668503nike-basketball', 4, 'Nike Basketball', 0, '6,12', 'Buy n Get n', 0, '2015-05-15 12:00:00', '2015-05-31 11:51:00', '<p>Wow nike ada promo lagi, ayo buruan serbu sekarang juga sebelum kehabisan</p><div  id "__if72ru4sdfsdfrkjahiuyi_once">&nbsp;</div><div  id="__hggasdgjhsagd_once">&nbsp;</div>', 'nikebasket4.jpg', 'nike-basketball', '2015-05-15 12:41:43', '2015-05-15 12:41:43'),
('1431674029promo-spesial-electronic-', 11, 'Promo Spesial Electronic City', 0, '99', 'Discount', 0, '2015-05-17 09:00:00', '2015-05-23 21:00:00', '<p>Promo minggu ini banyak product turun harga !!!</p>', 'promo-spesial-Electronic-City1.jpg', 'promo-spesial-electronic-', '2015-05-15 14:13:49', '2015-05-15 14:13:49'),
('PO_1430928985asc-promo', 3, 'Asc Promo', 0, '50', 'Discount', 50, '2015-05-10 23:14:00', '2015-06-27 23:14:00', '<p>Cari gadget pilihan kamu disini dan dapatkan diskon yang menarik</p>', 'promo_komputer.jpg', 'asc-promo', '2015-05-06 18:16:25', '2015-05-06 18:16:25');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `page` varchar(255) NOT NULL,
  `status` enum('non-active','active') NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `image`, `page`, `status`, `created`, `updated`) VALUES
(42, 'url1.jpg', 'Home,Elektronik', 'active', '2015-05-15 16:27:05', '2015-05-15 16:27:05'),
(44, 'promo20151.jpg', 'Home,Elektronik,Fashion,Kuliner', 'active', '2015-05-15 16:27:44', '2015-05-15 16:27:44'),
(45, 'bundling1.jpg', 'Elektronik', 'active', '2015-05-15 16:27:55', '2015-05-15 16:27:55'),
(46, 'rmd2.jpg', 'Fashion', 'active', '2015-05-15 16:54:04', '2015-05-15 16:54:04');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE IF NOT EXISTS `store` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `category_id` int(3) NOT NULL,
  `level` int(2) NOT NULL,
  `address` text NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `name`, `category_id`, `level`, `address`, `longitude`, `latitude`, `description`, `image`, `slug`, `created`, `updated`) VALUES
(1, 'KFC', 3, 1, 'Jalan Kaliurang KM 8.5', '110.38389560478515', '-7.758470173341815', '<p>KFC (dulu dikenal dengan nama Kentucky Fried Chicken) adalah suatu merek dagang waralaba dari Yum! Brands, Inc., yang bermarkas di Louisville, Kentucky, Amerika Serikat. Didirikan oleh Col. Harland Sanders, KFC dikenal terutama karena ayam gorengnya, yang biasa disajikan dalam &quot;timba&quot; (bucket) dari kertas karton.</p>', 'Logo_KFC1.png', 'kfc', '2015-05-06 16:35:41', '2015-05-12 13:07:42'),
(2, 'Sport Station', 2, 2, 'Jalan Tegal Mlati no 215', '110.368488', '-7.7472015', '<p>Sport Station menyediakan berbagai perlengkapan olahraga. Di Sport Station, Anda dapat menemukan pakaian olah raga, sepatu, raket dan peralatan olahraga lain dari berbagai merk yang sudah tidak asing lagi. Sport Station secara rutin mengadakan promo untuk menarik minat para calon pembeli membeli barang di tempat ini. Pelayanan di Sport Station juga memuaskan.&nbsp;</p>', 'sport_station.jpg', 'sport-station', '2015-05-06 17:57:09', '2015-05-06 17:58:01'),
(3, 'ASC', 1, 1, 'Jalan AM Sangaji no 1889', '110.369089', '-7.7665493', '<p>Toko Komputer-Notebook-Tablet terlengkap di Jogjakarta</p>', 'Asc.jpg', 'asc', '2015-05-06 18:13:01', '2015-05-10 02:58:55'),
(4, 'Nike', 2, 3, 'Jalan Gejayan nomor 1261', '110.377114', '-7.7776473', '<p>blablablablablaa</p>', 'Nike-do-it.jpg', 'nike', '2015-05-06 18:50:13', '2015-05-06 18:50:13'),
(5, 'Dixie Easy Dining', 3, 2, 'Jalan Gejayan nomor 301', '110.390289', '-7.7706739', '<p>Dixie merupakan salah satu tempat makan sekaligus nongkrong yang favorit di Jogjakarta.</p>', 'dixie.jpg', 'dixie-easy-dining', '2015-05-08 06:25:00', '2015-05-08 06:25:00'),
(6, 'Sushi Tei', 3, 2, 'Jalan Urip Sumaharjo 2461', '110.385783', '-7.7830049', '<p>Sushi adalah satu diantara bentuk sajian makanan khas Jepang yang mempunyai banyak penikmat di sejumlah kota besar di tanah air. Wajar saat restoran Sushi Tei dibuka pertama kalinya, antusiasme para penggemar Sushi begitu tinggi. Jaringan restoran makanan Jepang dari Singapura ini membuka gerai pertama di Jakarta pada tahun 2003 untuk penggemar sushi di Indonesia . Sejak itu respon masyarakat begitu bagus dan akhirnya membuka gerai baru di kota-kota besar lainnya di Indonesia : Bandung , Surabaya , Medan dan Bali serta menambah kehadirannya di Jakarta . Saat ini total ada 19 outlet Sushi Tei, dengan 10 diantaranya tersebar di Jabodetabek</p>', 'sushitei.jpg', 'sushi-tei', '2015-05-08 07:00:09', '2015-05-08 07:00:09'),
(7, 'Alfamart', 3, 3, 'Jalan Kalimantan 67', '110.382865', '-7.7495828', '<p>Alfamart adalah suatu perusahaan yang bergerak di bidang bisnis waralaba swalayan yang menjual berbagai jenis barang keperluan sehari-hari, dengan nama unggulan Alfa. Sebagian sahamnya saat ini dimiliki oleh PT. Sigmantara Alfindo.</p>', 'Logo-alfamart.jpg', 'alfamart', '2015-05-08 13:08:28', '2015-05-10 02:58:39'),
(8, 'Eiger Adventure', 2, 3, 'Jalan Magelang km 5 nomor 48', '', '', '<p>Perusahaan kami memproduksi tas dan peralatan petualangan, yang mana terbagi dalam tiga brand utama, yakni Eiger dengan positioning gaya hidup berpetualang (lifestyle adventure), Bodypack dengan positioning e-lifestyle, dan Nordwand dengan positioning kehidupan alam terbuka (outdoor living). Brand kami dikenal luas sebagai brand lokal yang sangat terkemuka di Indonesia. Pada tahun 2009, kami terdaftar sebagai Top 250 Indonsia Original Brands oleh sebuah majalah bisnis terkenal, Swa. Hal tersebut membuktikan kerja keras, tekad kuat, dan komitmen kami dari waktu ke waktu dalam rangka meraih kualitas unggul dan nama baik.</p>', 'eiger.jpg', 'eiger-adventure', '2015-05-08 13:21:35', '2015-05-08 13:21:35'),
(9, 'Kalimilk', 3, 3, 'Jalan Kaliurang Km 5 131', '110.380805', '-7.7607238', '<p>Kalimilk merupakan tempat nongkrong yang digemari oleh anak muda Jogjakarta. Di lihat dari namanya &quot;Kalimilk&quot; sudah pasti yang khas dari cafe ini adalah SUSU :)</p>', 'kalimilk.jpg', 'kalimilk', '2015-05-08 13:38:05', '2015-05-08 13:38:05'),
(10, 'McD', 3, 2, 'Jalan Jendral Sudirman no 327', '110.371407', '-7.7830899', '<p>McDonald&nbsp;(di Indonesia terkenal dengan sebutan McD, dibaca Mek-de) adalah rangkaian rumah makan siap saji terbesar di internasional. Hidangan utama di restoran-restoran McDonald&#39;s adalah hamburger, namun mereka juga menyajikan minuman ringan, kentang goreng dan hidangan-hidangan gan sa yang disesuaikan dengan tempat restoran itu berada. Lambang McDonald&#39;s adalah dua busur berwarna kuning yang biasanya dipajang di luar rumah-rumah makan mereka dan dapat segera dikenali oleh masyarakat luas.</p>', 'mcd.jpg', 'mcd', '2015-05-08 14:05:16', '2015-05-08 14:05:16'),
(11, 'Electronic City', 1, 3, 'Jogja City Mall, Jalan Magelang KM.5,8, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55284, Indonesia', '110.36033508079834', '-7.753154827872727', '<p><em>Electronic City</em> toko pertama ritel modern elektronik hadir dalam toko online aman dan terpercaya belanja elektronik lebih mudah dan praktis.</p>', 'Electronic-City1.png', 'electronic-city', '2015-05-15 13:54:38', '2015-05-15 13:56:18');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `log_product`
--
ALTER TABLE `log_product`
  ADD CONSTRAINT `log_product_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Constraints for table `log_promo`
--
ALTER TABLE `log_promo`
  ADD CONSTRAINT `log_promo_ibfk_1` FOREIGN KEY (`id_promo`) REFERENCES `promo` (`id`);

--
-- Constraints for table `log_store`
--
ALTER TABLE `log_store`
  ADD CONSTRAINT `log_store_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_promo`) REFERENCES `promo` (`id`);

--
-- Constraints for table `promo`
--
ALTER TABLE `promo`
  ADD CONSTRAINT `promo_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
