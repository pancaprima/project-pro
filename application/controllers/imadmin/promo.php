<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Promo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_promo');
        $this->load->model('m_store');
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->load->library('upload', $config);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "Promodia - Promo",
                'pos_parent'  =>  "promo",
                'pos_child'   =>  "list",
                'title'       =>  "Promo",
                'data'        =>  $this->m_promo->getAll(), 
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("promo"),
                'content'     =>  'imadmin/promo/list'
              );
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    public function detail($id='') {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "Promodia - Promo",
                'pos_parent'  =>  "promo",
                'pos_child'   =>  "list",
                'title'       =>  "Promo",
                'data'        =>  $this->m_promo->getAll(),
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("promo"),
                'data'        =>  $this->m_promo->getDetail($id),
                'action'      =>  "<a href='".base_url()."imadmin/promo' class='button button-blue' '><i class='fa fa-thumb-tack'></i> Exit </a>",
                'content'     =>  'imadmin/promo/detail'
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }


    public function add() {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_message('is_unique', 'Permalink already exist.');
            $this->form_validation->set_rules('title', 'Tama', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean|is_unique[promo.slug]');
            $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('start_date', 'Start_date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('end_date', 'End_date', 'trim|required|xss_clean');
            
            if ($this->form_validation->run() == FALSE) {
                $data= array (
                  'pagetitle'   =>  "Promodia - Promo",
                  'pos_parent'  =>  "promo",
                  'pos_child'   =>  "add",
                  'title'       =>  "Tambah Promo",
                  'subtitle'    =>  "",
                  'data'        =>  $this->m_store->getAll(),
                  'plugins_css' =>  array('assets/plugins/datetime-picker/jquery.datetimepicker.css'),
                  'plugins_js'  =>  array('assets/plugins/datetime-picker/jquery.datetimepicker.js'),
                  'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/promo/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                  'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/promo/">Promo</a>','add'),
                  'content'     =>  'imadmin/promo/add'
                );
                $this->load->view('imadmin/template/page', $data);
            }else {

                //var untuk cek apakah promo untuk semua item
                $all_items=$this->input->post('items');

                //create id_promo
                if($all_items=='n') $id_promo=time().$this->input->post('permalink');
                else $id_promo="PO_".time().$this->input->post('permalink');// PO artinya Promo Only
                
                //var untuk cek apakah promo untuk semua item
                $all_items=$this->input->post('items');
                $level= $this->input->post('level');
                $data = array(
                    'id'            => $id_promo,
                    'id_store'      => $this->input->post('id_store'),
                    'title'         => $this->input->post('title'),
                    'description'   => $this->input->post('description'),
                    'start_date'    => $this->input->post('start_date'),
                    'end_date'      => $this->input->post('end_date'),
                    'slug'          => $this->input->post('permalink'),
                    'created'       => date('Y-m-d H:i:s'),
                    'updated'       => date('Y-m-d H:i:s')
                );



                //cek berapa jumlah file image
                $files=$_FILES;
                $c =count($_FILES['image']['name']);
                //echo $c;
                //print_r($_FILES['image']['name']);
                if (empty($_FILES['image']['name'][1])){
                    $c=0;
                }
                //die();
                if($c>1){

                    $_FILES['image']['name']= $files['image']['name'][0];
                    $_FILES['image']['type']= $files['image']['type'][0];
                    $_FILES['image']['tmp_name']= $files['image']['tmp_name'][0];
                    $_FILES['image']['error']= $files['image']['error'][0];
                    $_FILES['image']['size']= $files['image']['size'][0];
                    if($_FILES['image']['size']< 2000000){
                                $dimension  = getimagesize($_FILES['image']['tmp_name']);
                                $width      = $dimension[0];
                                $height     = $dimension[1];
                                $img_confirm= $width / $height;
                                if($img_confirm==1){
                                    if (!$this->upload->do_upload("image")) {
                                        $data2 = array('error' => $this->upload->display_errors());
                                    } else {
                                        $data2 = array('upload_data' => $this->upload->data());
                                        $featured_image = $data2['upload_data']['file_name'];
                                    }
                                    $data['image'] = $featured_image;
                                }else{
                                    $this->session->set_flashdata("pesan", "<div class='alert alert-error'>
    	                <p><b>Your Photo Dimension not Valid!</b> recommended dimension 1:1 (width : height)<i class='fa fa-times'></i></p>
    	                </div>");
                                    redirect(base_url().'imadmin/promo/add', 'refresh');
                                }
                    }
                    $_FILES['image']['name']= $files['image']['name'][1];
                    $_FILES['image']['type']= $files['image']['type'][1];
                    $_FILES['image']['tmp_name']= $files['image']['tmp_name'][1];
                    $_FILES['image']['error']= $files['image']['error'][1];
                    $_FILES['image']['size']= $files['image']['size'][1];
                    if($_FILES['image']['size']< 2000000){
                        $dimension  = getimagesize($_FILES['image']['tmp_name']);
                        $width      = $dimension[0];
                        $height     = $dimension[1];
                        $img_confirm= $width / $height;
                        if($img_confirm==2){
                            if (!$this->upload->do_upload("image")) {
                                $data2 = array('error' => $this->upload->display_errors());
                            } else {
                                $data2 = array('upload_data' => $this->upload->data());
                                $featured_image = $data2['upload_data']['file_name'];
                            }
                            $data['image2'] = $featured_image;
                        }else{
                            $this->session->set_flashdata("pesan2", "<div class='alert alert-error'>
    	                <p><b>Your Photo Dimension not Valid!</b> recommended dimension 2:1 (width : height)<i class='fa fa-times'></i></p>
    	                </div>");
                            redirect(base_url().'imadmin/promo/add', 'refresh');
                        }
                    }
                }else{
                    $_FILES['image']['name']= $files['image']['name'][0];
                    $_FILES['image']['type']= $files['image']['type'][0];
                    $_FILES['image']['tmp_name']= $files['image']['tmp_name'][0];
                    $_FILES['image']['error']= $files['image']['error'][0];
                    $_FILES['image']['size']= $files['image']['size'][0];
                    if($_FILES['image']['size']< 2000000){
                        $dimension  = getimagesize($_FILES['image']['tmp_name']);
                        $width      = $dimension[0];
                        $height     = $dimension[1];
                        $img_confirm= $width / $height;
                        if($img_confirm==1){
                            if (!$this->upload->do_upload("image")) {
                                $data2 = array('error' => $this->upload->display_errors());
                            } else {
                                $data2 = array('upload_data' => $this->upload->data());
                                $featured_image = $data2['upload_data']['file_name'];
                            }
                            $data['image'] = $featured_image;
                        }else{
                            $this->session->set_flashdata("pesan", "<div class='alert alert-error'>
    	                <p><b>Your Photo Dimension not Valid!</b> recommended dimension 1:1 (width : height)<i class='fa fa-times'></i></p>
    	                </div>");
                            redirect(base_url().'imadmin/promo/add', 'refresh');
                        }
                    }
                }
                //echo "<br/>";
                //echo "MAAF, SCRIPT BELUM SELESAI! h3h3h3";
                //die();


                //start image validation
                /*
                if ($_FILES['image']['size'] != 0) {
                    if($_FILES['image']['size'] < 2000000){

                        $dimension  = getimagesize($_FILES['image']['tmp_name']);
                        $width      = $dimension[0];
                        $height     = $dimension[1];
                        $img_confirm= $width / $height;
                        if($img_confirm==1) {
                            if (!$this->upload->do_upload("image")) {
                                $data2 = array('error' => $this->upload->display_errors());
                            } else {
                                $data2 = array('upload_data' => $this->upload->data());
                                $featured_image = $data2['upload_data']['file_name'];
                            }
                            $data['image'][] = $featured_image;
                        }else{
                            $this->session->set_flashdata("pesan", "<div class='alert alert-error'>
    	                <p><b>Your Photo Dimension not Valid!</b> recommended dimension 1:1 (width : height)<i class='fa fa-times'></i></p>
    	                </div>");
                            redirect(base_url().'imadmin/store/add', 'refresh');
                        }

                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                        redirect(base_url().'imadmin/store/', 'refresh');
                    }
                } */
// end image validation

                
                if($all_items=='y'){
                    $data['promo_type'] =  $this->input->post('promo_type');
     
                    //hitung besar diskon
                    if($data['promo_type']=="Discount") $data['discount'] = $this->input->post('discount');
                    else if($data['promo_type']=="Buy n Get n"){
                        $buy = $this->input->post('buy');
                        $get = $this->input->post('get');
                        
                        $data['discount']   =  round($buy*100/$get);
                        $data['discount']   = $data['discount'].".".$buy;
                    }
                }
                
                $this->m_promo->insert($data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                  <p><b>Success!</b> Post '".$this->input->post('title')."' was created.<i class='fa fa-times'></i></p>
              </div>");
                $action=$this->input->post('button-simpan');
                
                if($action == 'Simpan dan Tambah Produk'){
                    redirect(base_url().'imadmin/product/add?id_promo='.$id_promo, 'refresh');
                }else{
                    redirect(base_url().'imadmin/promo/', 'refresh');    
                }
                
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    public function edit($id='') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_message('is_unique', 'Permalink already exist.');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean|is_unique[promo.slug]');
            $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('start_date', 'Start_date', 'trim|required|xss_clean');
            $this->form_validation->set_rules('end_date', 'End_date', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data= array (
                  'pagetitle'   =>  "Promodia - Promo",
                  'pos_parent'  =>  "promo",
                  'pos_child'   =>  "edit",
                  'title'       =>  "Ubah Promo",
                  'subtitle'    =>  "",
                  'data'        =>  $this->m_promo->getDetail($id),
                  'plugins_css' =>  array('assets/plugins/datetime-picker/jquery.datetimepicker.css'),
                  'plugins_js'  =>  array('assets/plugins/datetime-picker/jquery.datetimepicker.js'),
                  'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/promo/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                  'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/promo/">Promo</a>','edit'),
                  'content'     =>  'imadmin/promo/edit'
                );
                $this->load->view('imadmin/template/page', $data);
            }else {
                
                if (!$this->upload->do_upload("image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $featured_image = $data['upload_data']['file_name'];
                }
                
                 $data = array(
                    'title'         => $this->input->post('title'),
                    'description'   => $this->input->post('description'),
                    'start_date'    => $this->input->post('start_date'),
                    'end_date'      => $this->input->post('end_date'),
                    'slug'          => $this->input->post('permalink'),
                    'updated'       => date('Y-m-d H:i:s')
                );
                
                
                if ($_FILES['image']['size'] != 0) {
                    if($_FILES['image']['size'] < 2000000){
                        $data['image'] = $featured_image;
                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                
                        redirect(base_url().'imadmin/promo/add', 'refresh'); 
                    }
                    
                }
                
                
                //var untuk cek apakah promo untuk semua item
                $all_items=$this->input->post('items');
                
                if($all_items=='y'){
                    $data['promo_type'] =  $this->input->post('promo_type');
     
                    //hitung besar diskon
                    if($data['promo_type']=="Discount") $data['discount'] = $this->input->post('discount');
                    else if($data['promo_type']=="Buy n Get n"){
                        $buy = $this->input->post('buy');
                        $get = $this->input->post('get');
                        
                        $data['discount']   =  round($buy*100/$get);
                        $data['discount']   = $data['discount'].".".$buy;    
                    }
                }

                $this->m_promo->update($id,$data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Edit Success!</b> Promo '".$data['title']."' was created.<i class='fa fa-times'></i></p>
	            </div>");
                
                redirect(base_url().'imadmin/promo', 'refresh');
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    
    

    function delete($id=''){
        if ($this->session->userdata('loggedin')) {
            $this->m_promo->delete($id);

            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Data successfully deleted from database.<i class='fa fa-times'></i></p>
            </div>");
            redirect(base_url().'imadmin/promo', 'refresh');
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }


}
