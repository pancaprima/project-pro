<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Product extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_product');
        $this->load->model('m_promo');
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->load->library('upload', $config);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "Promodia - Product",
                'pos_parent'  =>  "product",
                'pos_child'   =>  "list",
                'title'       =>  "Product",
                'data'        =>  $this->m_product->getAll(), 
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("product"),
                'content'     =>  'imadmin/product/list'
              );
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
     public function detail($id='') {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "Promodia - Product",
                'pos_parent'  =>  "product",
                'pos_child'   =>  "list",
                'title'       =>  "Product",
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("product"),
                'data'        =>  $this->m_product->getDetail($id),
                'action'      =>  "<a href='".base_url()."imadmin/product' class='button button-blue' '><i class='fa fa-thumb-tack'></i> Exit </a>",
                'content'     =>  'imadmin/product/detail'
            );
            
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('nama_produk', 'nama_produk', 'trim|required|xss_clean');
            $this->form_validation->set_rules('description', 'description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('promo_type', 'promo_type', 'trim|required|xss_clean');
            $this->form_validation->set_rules('harga_normal', 'normal_price', 'trim|required|xss_clean');
            $this->form_validation->set_rules('tag', 'tag', 'trim|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                
                $data= array (
                      'pagetitle'   =>  "Promodia - Product",
                      'pos_parent'  =>  "product",
                      'pos_child'   =>  "add",
                      'title'       =>  "Tambah Produk",
                      'subtitle'    =>  "",
                      'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/product/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                      'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/product/">Product</a>','add'),
                      'content'     =>  'imadmin/product/add'
                );
                    
                if($this->input->get('id_promo')!=null){
                    $id_promo=$this->input->get('id_promo');                   
                    $data['data']=  $this->m_promo->getDetail($id_promo); //get promo tersebut                 
                }else{
                    $data['data']=  $this->m_promo->getAll(); //get semua promo
                }
                
                $this->load->view('imadmin/template/page', $data);
            }else {

                $data = array(
                    'id_promo'      => $this->input->post('id_promo'),
                    'name'          => $this->input->post('nama_produk'),
                    'description'   => $this->input->post('description'),
                    'promo_type'    => $this->input->post('promo_type'),
                    'normal_price'  => $this->input->post('harga_normal'),
                    'tag'           => $this->input->post('tag'),
                    'created'       => date('Y-m-d H:i:s'),
                    'updated'       => date('Y-m-d H:i:s')
                );
                 
                //hitung besar diskon
                if($data['promo_type']=="Discount"){
                  $data['discount'] = $this->input->post('discount');
                  $data['promo_factor'] = $data['discount'];  
                } 
                else if($data['promo_type']=="Buy n Get n"){
                    $buy = $this->input->post('buy');
                    $get = $this->input->post('get');
                     
                    $data['discount']   =  round($buy*100/$get); 
                    $data['promo_factor'] = $buy.",".$get;  
                }else if ($data['promo_type']=="New Price"){
                    $harga_promo = $this->input->post('harga_promo');
                    $harga_normal = $this->input->post('harga_normal');
                    
                    $data['discount'] = round(($harga_normal-$harga_promo)/$harga_normal*100); 
                    $data['promo_factor'] = $harga_promo; 
                }else if ($data['promo_type']=="Free Stuff"){
                    $harga_freestuff = $this->input->post('harga_freestuff');
                    $harga_normal = $this->input->post('harga_normal');
                    
                    $data['discount'] = round($harga_freestuff/$harga_normal*100);
                    $data['promo_factor'] = $harga_freestuff;
                }
                 
                //add gambar jika ada gambar 
                if($_FILES['image']['size']< 2000000){
                    $dimension  = getimagesize($_FILES['image']['tmp_name']);
                    $width      = $dimension[0];
                    $height     = $dimension[1];
                    $img_confirm= $width / $height;
                    if($img_confirm==1){
                        if (!$this->upload->do_upload("image")) {
                            $data2 = array('error' => $this->upload->display_errors());
                        } else {
                            $data2 = array('upload_data' => $this->upload->data());
                            $featured_image = $data2['upload_data']['file_name'];
                        }
                        $data['image'] = $featured_image;
                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-error'>
    	                <p><b>Your Photo Dimension not Valid!</b> recommended dimension 1:1 (width : height)<i class='fa fa-times'></i></p>
    	                </div>");
                        redirect(base_url().'imadmin/product/add', 'refresh');
                    }
                }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                
                        redirect(base_url().'imadmin/product/add', 'refresh');
                    }
                    




                $biggestDiscount = $this->m_product->getBiggestDiscount($data['id_promo']);
                
                //simpan value tombol submit
                $submitButton= $this->input->post('submitButton');
                
                $this->m_product->insert($data);
                

                if($data['discount']>$biggestDiscount){
                    $dataDiscount = array(
                        'discount' => $data['discount'],
                        'promo_type' => $data['promo_type'],
                        'promo_factor' => $data['promo_factor']
                        );
                    $this->m_promo->update($data['id_promo'],$dataDiscount);
                }


                if($submitButton=="Simpan"){
                    redirect(base_url().'imadmin/product', 'refresh');
                }else{
                    redirect(base_url().'imadmin/product/add?id_promo='.$data['id_promo'], 'refresh');
                }
               
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    public function edit($id='') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('nama_produk', 'nama_produk', 'trim|required|xss_clean');
            $this->form_validation->set_rules('description', 'description', 'trim|required|xss_clean');
            $this->form_validation->set_rules('promo_type', 'promo_type', 'trim|required|xss_clean');
            $this->form_validation->set_rules('harga_normal', 'normal_price', 'trim|required|xss_clean');
            $this->form_validation->set_rules('tag', 'tag', 'trim|xss_clean');
            
            if ($this->form_validation->run() == FALSE) {
                
                 $data= array (
                      'pagetitle'   =>  "Promodia - Product",
                      'pos_parent'  =>  "product",
                      'pos_child'   =>  "edit",
                      'title'       =>  "Ubah Produk",
                      'data'        =>  $this->m_product->getDetail($id),
                      'subtitle'    =>  "",
                      'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/product/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                      'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/product/">Product</a>','edit'),
                      'content'     =>  'imadmin/product/edit'
                );
                
                $this->load->view('imadmin/template/page', $data);
            }else {
                
                if (!$this->upload->do_upload("image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $featured_image = $data['upload_data']['file_name'];
                }
                
                 $data = array(
                    'name'          => $this->input->post('nama_produk'),
                    'description'   => $this->input->post('description'),
                    'promo_type'    => $this->input->post('promo_type'),
                    'normal_price'  => $this->input->post('harga_normal'),
                    'tag'           => $this->input->post('tag'),
                    'updated'       => date('Y-m-d H:i:s')
                );
                
                if ($_FILES['image']['size'] != 0) {
                    if($_FILES['image']['size'] < 2000000){
                        $data['image'] = $featured_image;
                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                
                        redirect(base_url().'imadmin/product/', 'refresh'); 
                    }
                    
                } 
                
                
                 //hitung besar diskon
                if($data['promo_type']=="Discount"){
                  $data['discount'] = $this->input->post('discount');
                  $data['promo_factor'] = $data['discount'];  
                } 
                else if($data['promo_type']=="Buy n Get n"){
                    $buy = $this->input->post('buy');
                    $get = $this->input->post('get');
                     
                    $data['discount']   =  round($buy*100/$get); 
                    $data['promo_factor'] = $buy.",".$get;  
                }else if ($data['promo_type']=="New Price"){
                    $harga_promo = $this->input->post('harga_promo');
                    $harga_normal = $this->input->post('harga_normal');
                    
                    $data['discount'] = round(($harga_normal-$harga_promo)/$harga_normal*100); 
                    $data['promo_factor'] = $harga_promo; 
                }else if ($data['promo_type']=="Free Stuff"){
                    $harga_freestuff = $this->input->post('harga_freestuff');
                    $harga_normal = $this->input->post('harga_normal');
                    
                    $data['discount'] = round($harga_freestuff/$harga_normal*100);
                    $data['promo_factor'] = $harga_freestuff;
                }
                
                 $id_promo = $this->input->post('id_promo');
                 $biggestDiscount = $this->m_product->getBiggestDiscount($id_promo);
                 
                
                 $this->m_product->update($id,$data);
                

                if($data['discount']>$biggestDiscount){
                    $dataDiscount = array(
                        'discount' => $data['discount'],
                        'promo_type' => $data['promo_type']
                        );
                    $this->m_promo->update($id_promo,$dataDiscount);
                }

                
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Edit Success!</b> Product '".$data['name']."' was created.<i class='fa fa-times'></i></p>
	            </div>");
                
                redirect(base_url().'imadmin/product', 'refresh');
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    function delete($id=''){
        if ($this->session->userdata('loggedin')) {
            $this->m_product->delete($id);

            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Data successfully deleted from database.<i class='fa fa-times'></i></p>
            </div>");
            redirect(base_url().'imadmin/product', 'refresh');
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

}
