<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Category extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_category', '',TRUE);
        $this->load->helper(array('form'));
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "ProjectPro - Category Management",
                'pos_parent'  =>  "store",
                'pos_child'   =>  "category",
                'title'       =>  "Category Management",
                'data'        =>  $this->m_category->getAll(),
                'plugins_css' =>  array('assets/plugins/jqueryui-editable/css/jqueryui-editable.css'),
                'plugins_js'  =>  array('assets/plugins/jqueryui-editable/js/jqueryui-editable.js'),
                'subtitle'    =>  "",
                'breadcrumb'  =>   array('<a href="'.base_url().'imadmin/store/">Store</a>','category management'),
                'content'     =>  'imadmin/category/add'
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {

                $data = array(
                        'name'          => $this->input->post('name'),
                        'created'       => date('Y-m-d H:i:s'),
                        'updated'       => date('Y-m-d H:i:s'),
                    );

            
                $this->m_category->insert($data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Success!</b> Post '".$this->input->post('name')."' was created.<i class='fa fa-times'></i></p>
	            </div>");
                redirect(base_url().'imadmin/category', 'refresh');
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    function delete($id=''){
        if ($this->session->userdata('loggedin')) {
            $this->m_category->delete($id);

            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Data successfully deleted from database.<i class='fa fa-times'></i></p>
            </div>");
            redirect(base_url().'imadmin/category', 'refresh');
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    public function edit($id='') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_rules('name', 'name', 'trim|required|xss_clean');
            
            if ($this->form_validation->run() == FALSE) {
                
                 $data= array (
                      'pagetitle'   =>  "Promodia - Category",
                      'pos_parent'  =>  "category",
                      'pos_child'   =>  "edit",
                      'title'       =>  "Ubah Category",
                      'data'        =>  $this->m_category->getDetail($id),
                      'subtitle'    =>  "",
                      'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/category/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                      'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/category/">Category</a>','edit'),
                      'content'     =>  'imadmin/category/edit'
                );
                
                $this->load->view('imadmin/template/page', $data);
            }else {
                
                if (!$this->upload->do_upload("image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $featured_image = $data['upload_data']['file_name'];
                }
                
                 $data = array(
                    'name'          => $this->input->post('name'),
                    'updated'       => date('Y-m-d H:i:s')
                );
                
                if ($_FILES['image']['size'] != 0) {
                    if($_FILES['image']['size'] < 2000){
                        $data['image'] = $featured_image;
                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                        <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
                        </div>");
                
                        redirect(base_url().'imadmin/category/', 'refresh'); 
                    }
                    
                } 
                
                

                
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                    <p><b>Edit Success!</b> Category '".$data['name']."' was created.<i class='fa fa-times'></i></p>
                </div>");
                
                redirect(base_url().'imadmin/category', 'refresh');
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

}
