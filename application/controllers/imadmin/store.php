<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Store extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_store', '',TRUE);
        $this->load->model('m_category', '',TRUE);
        $this->load->helper(array('form'));
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $this->load->library('upload', $config);
        $this->load->library('googlemaps');
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "ProjectPro - Store",
                'pos_parent'  =>  "store",
                'pos_child'   =>  "list",
                'title'       =>  "Store",
                'data'        =>  $this->m_store->getAll(), 
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("store"),
                'content'     =>  'imadmin/store/list'
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    public function detail($id='') {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "ProjectPro - Store",
                'pos_parent'  =>  "store",
                'pos_child'   =>  "list",
                'title'       =>  "Store",
                'data'        =>  $this->m_store->getAll(),
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("store"),
                'data'        =>  $this->m_store->getDetail($id),
                'action'      =>  "<a href='".base_url()."imadmin/store' class='button button-blue' '><i class='fa fa-thumb-tack'></i> Exit </a>",
                'content'     =>  'imadmin/store/detail'
            );
            $coba=$this->m_store->getDetail($id);
            foreach ($coba as $cc ) {
                $lt=$cc->latitude;
                $lng=$cc->longitude;
            }
            
                $map_config = array();
                $map_config['center'] = $lt.','.$lng;
                
                $map_config['zoom'] = '15';
               // $config['onclick'] = '$("#lat").val(event.latLng.lat()) ; $("#long").val(event.latLng.lng()) ;';
                $this->googlemaps->initialize($map_config);
                
                    $marker = array();
                    $marker['position'] = $lt.','.$lng;
                    $marker['draggable'] = false;
                    $this->googlemaps->add_marker($marker);
                $data['map'] = $this->googlemaps->create_map();
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_message('is_unique', 'Permalink already exist.');
            $this->form_validation->set_rules('nama', 'Nama', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean|is_unique[promo.slug]');
            $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
            $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data= array (
                  'pagetitle'   =>  "ProjectPro - Store",
                  'pos_parent'  =>  "store",
                  'pos_child'   =>  "add",
                  'title'       =>  "Tambah Store",
                  'category'    =>  $this->m_category->getAll(),
                  'subtitle'    =>  "",
                  'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/store/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                  'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/store/">Store</a>','add'),
                  'content'     =>  'imadmin/store/add'
                );
                
                $map_config = array();
                $map_config['center'] = '-7.7547707,110.3808057';              
                $map_config['zoom'] = '15';
             // $map_config['onclick'] = '$("#lat").val(event.latLng.lat()) ; $("#long").val(event.latLng.lng()) ;';
                $this->googlemaps->initialize($map_config);

                $marker = array();
                $marker['position'] = '-7.7547707,110.3808057';
                $marker['draggable'] = true;
                $marker['ondragend'] = '$("#lat").val(event.latLng.lat()) ; $("#long").val(event.latLng.lng()) ;';
                $this->googlemaps->add_marker($marker);
                $data['map'] = $this->googlemaps->create_map();
                
                $this->load->view('imadmin/template/page', $data);
            }else {

                
                //store name checking
                $restrictedName = $this->m_category->getAll();
                foreach($restrictedName as $rn){
                    if(strtolower($rn->name)==strtolower($this->input->post('nama'))){
                         $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Nama store dilarang!</b><i class='fa fa-times'></i></p>
    	                </div>");
                
                        redirect(base_url().'imadmin/store/', 'refresh'); 
                    }
                }
                
                $data_ = array(
                        'name'          => $this->input->post('nama'),
                        'category_id'   => $this->input->post('kategori'),
                        'level'         => $this->input->post('level'),
                        'address'       => $this->input->post('alamat'),
                        'latitude'      => $this->input->post('lat'),
                        'longitude'     => $this->input->post('long'),
                        'description'   => $this->input->post('deskripsi'),
                        'slug'          =>  $this->input->post('permalink'),
                        'created'       => date('Y-m-d H:i:s'),
                        'updated'       => date('Y-m-d H:i:s'),
                    );

//start image validation
                if ($_FILES['image']['size'] != 0) {
                    if($_FILES['image']['size'] < 2000000){

                        $dimension  = getimagesize($_FILES['image']['tmp_name']);
                        $width      = $dimension[0];
                        $height     = $dimension[1];
                        $img_confirm= $width / $height;
                        if($img_confirm==2) {
                            if (!$this->upload->do_upload("image")) {
                                $data = array('error' => $this->upload->display_errors());
                            } else {
                                $data = array('upload_data' => $this->upload->data());
                                $featured_image = $data['upload_data']['file_name'];
                            }
                            $data_['image'] = $featured_image;
                        }else{
                            $this->session->set_flashdata("pesan", "<div class='alert alert-error'>
    	                <p><b>Your Photo Dimension not Valid!</b> recommended dimension 2:1 (width : height)<i class='fa fa-times'></i></p>
    	                </div>");
                            redirect(base_url().'imadmin/store/add', 'refresh');
                        }

                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                        redirect(base_url().'imadmin/store/', 'refresh'); 
                    }
                }
// end image validation
                
                $this->m_store->insert($data_);
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Success!</b> Post '".$this->input->post('title')."' was created.<i class='fa fa-times'></i></p>
	            </div>");
                redirect(base_url().'imadmin/store', 'refresh');
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }
    
    public function edit($id='') {
        if ($this->session->userdata('loggedin')) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            $this->form_validation->set_message('is_unique', 'Permalink already exist.');
            $this->form_validation->set_rules('nama', 'Nama', 'trim|required|xss_clean');
            $this->form_validation->set_rules('permalink', 'Permalink', 'trim|required|xss_clean|is_unique[promo.slug]');
            $this->form_validation->set_rules('level', 'Level', 'trim|required|xss_clean');
            $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
            $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data= array (
                  'pagetitle'   =>  "ProjectPro - Store",
                  'pos_parent'  =>  "store",
                  'pos_child'   =>  "edit",
                  'title'       =>  "Edit Store",
                  'subtitle'    =>  "",
                  'data'        =>  $this->m_store->getDetail($id),
                  'category'    =>  $this->m_category->getAll(),
                  'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/store/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                  'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/store/">Store</a>','edit'),
                  'content'     =>  'imadmin/store/edit'
                );
                
                $data['data'][0]->category=$this->m_category->getDetail($data['data'][0]->category_id);
                $map_config = array();
                $map_config['center'] = $data['data'][0]->latitude.','.$data['data'][0]->longitude;
                $map_config['zoom'] = '15';
             // $map_config['onclick'] = '$("#lat").val(event.latLng.lat()) ; $("#long").val(event.latLng.lng()) ;';
                $this->googlemaps->initialize($map_config);

                $marker = array();
                $marker['position'] = $data['data'][0]->latitude.','.$data['data'][0]->longitude;
                $marker['draggable'] = true;
                $marker['ondragend'] = '$("#lat").val(event.latLng.lat()) ; $("#long").val(event.latLng.lng()) ;';
                $this->googlemaps->add_marker($marker);
                $data['map'] = $this->googlemaps->create_map();
                
                $this->load->view('imadmin/template/page', $data);
            }else {
                
                if (!$this->upload->do_upload("image")) {
                    $data = array('error' => $this->upload->display_errors());
                } else {
                    $data = array('upload_data' => $this->upload->data());
                    $featured_image = $data['upload_data']['file_name'];
                    echo $featured_image;

                }
                
                $data = array(
                    'name'          => $this->input->post('nama'),
                    'category_id'   => $this->input->post('kategori'),
                    'level'         => $this->input->post('level'),
                    'address'       => $this->input->post('alamat'),
                    'latitude'      => $this->input->post('lat'),
                    'longitude'     => $this->input->post('long'),
                    'description'   => $this->input->post('deskripsi'),
                    'slug'          => $this->input->post('permalink'),
                    'updated'       => date('Y-m-d H:i:s')
                 );
                 
                if ($_FILES['image']['size'] != 0) {
                    if($_FILES['image']['size'] < 2000000){
                        $data['image'] = $featured_image;
                    }else{
                        $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                
                        redirect(base_url().'imadmin/store/', 'refresh'); 
                    }
                    
                } 

                $this->m_store->update($id,$data);
                $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Edit Success!</b> Store '".$data['name']."' was created.<i class='fa fa-times'></i></p>
	            </div>");
                
                redirect(base_url().'imadmin/store', 'refresh');
            }
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    function delete($id=''){
        if ($this->session->userdata('loggedin')) {
            $this->m_store->delete($id);

            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Data successfully deleted from database.<i class='fa fa-times'></i></p>
            </div>");
            redirect(base_url().'imadmin/store', 'refresh');
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

}
