<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
//we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Slider extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_slider', '',TRUE);;
        $this->load->model('m_category', '',TRUE);;
        $this->load->helper(array('form'));
        $config['upload_path'] = "./assets/uploads";
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->load->library('upload', $config);
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data= array (
                'pagetitle'   =>  "ProjectPro - Slider",
                'pos_parent'  =>  "slider",
                'pos_child'   =>  "list",
                'title'       =>  "Slider",
                'data'        =>  $this->m_slider->getAll(),
                'subtitle'    =>  "",
                'breadcrumb'  =>  array("slider"),
                'content'     =>  'imadmin/slider/'
            );
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    public function add() {
        if ($this->session->userdata('loggedin')) {

                $data = array (
                    'pagetitle'   =>  "ProjectPro - Slider",
                    'pos_parent'  =>  "slider",
                    'pos_child'   =>  "add",
                    'title'       =>  "Tambah Slider",
                    'data'        =>  $this->m_slider->getAll('all','w'),
                    'category'    =>  $this->m_category->getAll(),
                    'subtitle'    =>  "",
                    'action'      =>  "<a class='button button-red' href='".base_url()."imadmin/slider/'><i class='fa fa-trash-o'></i> Cancel and Discard</a>",
                    'breadcrumb'  =>  array('<a href="'.base_url().'imadmin/slider/">Slider</a>','add'),
                    'content'     =>  'imadmin/slider/add'
                );

                $this->load->view('imadmin/template/page', $data);
        } else {

            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

    public function prosesAdd(){
        if($this->session->userdata('loggedin')){
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters("class='form-error' title='", "'");
            
            if (!$this->upload->do_upload("image")) {
                $data = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
                $featured_image = $data['upload_data']['file_name'];
            }

            if ($_FILES['image']['size'] != 0) {
                if($_FILES['image']['size'] < 2000000){
                    $category = $this->input->post('category');
                    foreach ($category as $c) {
                        !isset($allcategory) ?  $allcategory = $c :  $allcategory = $allcategory.','.$c;
                    }
                    $data2 = array(
                        'page'          => $allcategory,
                        'created'       => date('Y-m-d H:i:s'),
                        'updated'       => date('Y-m-d H:i:s'),
                        'image'         => $featured_image
                    );
                    $this->m_slider->insert($data2);
                    $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
	                <p><b>Success!</b> Post '".$this->input->post('title')."' was created.<i class='fa fa-times'></i></p>
	            </div>");
                }else{
                    $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
    	                <p><b>Your Photo Size is too Big!</b> recommended size is under 2Mb.<i class='fa fa-times'></i></p>
    	                </div>");
                
                }

            }

            redirect(base_url().'imadmin/slider/add', 'refresh');

        }else{
            redirect('login','refresh');
        }
    }

   

    function status($id='',$stat=''){
        if ($this->session->userdata('loggedin')) {

             $data = array(
                    'status'      =>  $stat
                );

             $this->m_slider->update($id,$data);
                
                redirect(base_url().'imadmin/slider/add', 'refresh');

        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
        
    }

    function delete($id=''){
        if ($this->session->userdata('loggedin')) {
            $this->m_slider->delete($id);

            $this->session->set_flashdata("pesan", "<div class='alert alert-notice'>
                <p><b>Success!</b> Data successfully deleted from database.<i class='fa fa-times'></i></p>
            </div>");
            redirect(base_url().'imadmin/slider/add', 'refresh');
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

}
