<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 //we need to call PHP's session object to access it through CI

/* PANDUAN VARIABLE DI CONTROLLER
   - $data['pagetitle'] : buat ngasih title (di tab browser)
   - $data['pos_parent'] : untuk mendeteksi posisi halaman. pilihannya dashboard|peserta1|information|administration (menyesuaikan kebutuhan)
   - $data['pos_child'] : opsional. digunakan jika halaman yg dimaksud merupakan anak/child dari sebuah parent menu. (semoga mudeng)
   - $data['title'] : Untuk menentukan judul sebuah halaman. sebaiknya diawali huruf kapital
   - $data['subtitle'] : untuk menentukan subjudul suatu halaman
   - $data['action'] : opsional. digunakan jika ingin menambahkan tombol/link di sebelah kanan header
   - $data['breadcrumb'] : untuk membuat breadcrumb. tipe data array. nilai default 'array()'. contoh = array('<a>Administration</a>', 'Users')
   - $data['content'][] : kontent yang akan diload di halaman. berupa file view
*/

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('loggedin')) {
            $data['pagetitle'] = "Promodia - Dashboard";
            $data['pos_parent'] = "dashboard";

            $data['title'] = "Dashboard";
            $data['subtitle'] = "Selamat datang di halaman dashboard";
            $data['breadcrumb'] = array();
            $this->load->view('imadmin/template/page', $data);
        } else {
            //If no session, redirect to login page
            redirect(base_url().'imadmin/login', 'refresh');
        }
    }

}
