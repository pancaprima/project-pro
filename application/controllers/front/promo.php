<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('m_promo');
		$this->load->model('m_product');
		$this->load->model('m_store');
		$this->load->model('m_category');
		$this->load->model('m_slider');
		$this->load->library('googlemaps');
		$this->load->library('discountformatter');
		$this->load->library('endingcountdown');
	}

	public function index($cat = 'all') {
		$sortby = $this->input->get('sortby');
		if ($sortby == "") {
			$order = 'start_date';
		} else {
			$order = $sortby;
		}
		$data = array(
			'title'      => 'Promo | Promodia',
			'css'        => 'style-promo.css',
			'content'    => 'content/promo',
			'plugins_js' => array(
                'assets/plugins/isotope/isotope.min.js',
                'assets/plugins/bxslider/jquery.bxslider.js'
            ),
			'plugins_css'=> array(
                'assets/css/jquery.bxslider.css'
            ),
			'data_promo' => $this->m_promo->getPromo('category_id',$cat,$order,40,0),
			'data_store' => $this->m_store->getStore('category_id',$cat,'level',6,0),
			'promo_count'=> $this->m_promo->getNumPromo($cat),
			'sortby'     => $sortby,
			'category'   => $cat != 'all' ? $this->m_category->getCategoryName($cat) : 'Semua Promo',
			'meta'		 => array(
				'description'   => 'Jangan lewatkan promo-promo menarik dari Promodia!',
				'og:description'=> 'Jangan lewatkan promo-promo menarik dari Promodia!',
				'og:image'      => base_url().'assets/images/banner.jpg',
				'og:title'      => 'Promo menarik dari Promodia',
				'og:url'        => base_url().$cat,
				'og:site_name'  => 'Promodia',
				'og:type'       => 'website',
				'twitter:card'  => 'photo',
				'twitter:url'   => base_url().$cat,
				'twitter:title' => 'Promo menarik dari Promodia',
				'twitter:description' => 'Jangan lewatkan promo-promo menarik dari Promodia!',
				'twitter:image' => base_url().'assets/images/banner.jpg'
			)
		);
		$data['slider'] = $this->m_slider->getAll('page', $data['category']);
		$this->load->view('template/page', $data);
	}

	public function view($store, $promo) {
		$data_product=$this->m_product->getProduct('promo_slug', $promo);
		$data = array(
			'title'        => 'Project Pro',
			'css'          => 'style-promoview.css',
			'content'      => 'content/promo-view',
			'plugins_js'   => array(
                'assets/plugins/isotope/isotope.min.js',
                'assets/plugins/jquery.sticky-kit.min.js',
                'assets/plugins/fancybox/source/jquery.fancybox.js?v=2.1.5',
                'assets/plugins/fancybox/script.js'
            ),
            'plugins_css'  => array(
	            'assets/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5'
            ),	
			'data_promo'   => $this->m_promo->getPromoDetail($store, $promo),
			'data_product' => $this->discountformatter->set($data_product),
			'product_count'=> $this->m_product->getProductNum('promo_slug', $promo)
		);
	
		foreach ($data['data_promo'] as $p) {
			//set meta
			$data['meta'] = array(
				'description'   => strip_tags($p->description),
				'og:description'=> 'Klik untuk melihat promo dari '.$p->sname.' sebelum terlambat!',
				'og:image'      => base_url().'assets/uploads/'.$p->image,
				'og:title'      => $p->title,
				'og:url'        => base_url().$p->sslug.'/promo/'.$p->slug,
				'og:site_name'  => 'Promodia',
				'og:type'       => 'website',
				'twitter:card'  => 'photo',
				'twitter:url'   => base_url().$p->sslug.'/promo/'.$p->slug,
				'twitter:title' => $p->title,
				'twitter:description' => strip_tags($p->description),
				'twitter:image' => base_url().'assets/uploads/'.$p->image
			);
			
			//set type untuk halaman PO atau bukan
			$data['type']=substr($p->id,0,2);
				
		}
		$this->googlemaps->bikin_peta($data['data_promo'][0]->slatitude, $data['data_promo'][0]->slongitude, 15);
		$data['map'] = $this->googlemaps->create_map();
		$data['ending'] = $this->endingcountdown->set($data['data_promo'][0]->end_date);
		$this->load->view('template/page', $data);
	}
}