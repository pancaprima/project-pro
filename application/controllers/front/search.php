<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('m_store');
		$this->load->model('m_product');
		$this->load->model('m_promo');
		$this->load->model('m_category');
		$this->load->library('discountformatter');
		$this->load->library('endingcountdown');
	}

	public function index() {
		
		$data = array(
			'title'      => 'Pencarian | Promodia',
			'css'        => 'style-search.css',
			'content'    => 'content/search',
			'plugins_js' => array('assets/plugins/isotope/isotope.min.js','assets/plugins/jquery.sticky-kit.min.js','assets/js/jquery-ui.min.js'),
			'plugins_css' => array('assets/css/jquery-ui.css')
		);
		
		$search = array(
			'query'	=> $this->input->get('q')
		);
		
		$search['result']['store']= $this->m_store->search($search['query']);

		//jika hasil pencarian store ada satu, redirect ke halaman store
		if($search['result']['store']['num_rows'] == 1){
			redirect(base_url().$search['result']['store']['result'][0]->slug,'refresh');
		}
		
		//get all parameter in advance search
		$this->input->get('category')!="" ? $category = $this->input->get('category') : $category = "all";
		if($this->input->get('rangeLow')!=""){
			$range['low']	= $this->input->get('rangeLow');
			$range['high']	= $this->input->get('rangeHigh');	
		} else $range="all";
		$sortby=$this->input->get('sortby');
		$sortby == "" ? $order = 'start_date' :	$order = $sortby;
				
		$search['queryExploded'] = explode(" ",$search['query']);
		$search['result']['promo'] 		= $this->m_promo->search($search['queryExploded'],$category,$range,$order,1);
		$search['result']['product'] 	= $this->m_product->search($search['queryExploded'],$category,$range,$order,1);
		
		
		$search['result']['product']['result']=$this->discountformatter->set($search['result']['product']['result']);
		
		$data['data']= array(
			'store'				=>	$search['result']['store']['result'],
			'storeRows'			=>	$search['result']['store']['num_rows'],
			'promo' 			=>	$this->discountformatter->promo($search['result']['promo']['result']['result']),
			'promoLimitedRows'	=>	$search['result']['promo']['result']['num_rows'],
			'promoRows'			=>	$search['result']['promo']['num_rows'],
			'product'			=>	$search['result']['product']['result']['result'],
			'productLimitedRows'=>	$search['result']['product']['result']['num_rows'],
			'productRows'		=>	$search['result']['product']['num_rows'],
			'query'				=>	$search['query'],
			'range'				=>	$range,
			'categorySelected' 	=>  $category,
			'sortby'			=>	$sortby,
			'category'			=>	$this->m_category->getAll()
		);
		
		if ($data['data']['promoLimitedRows']) {
			foreach ($data['data']['promo'] as $pr) {
				$pr->ending = $this->endingcountdown->set($pr->end_date);
			}
		}
		// print_r($data['data']['promo']);
		// $data['ending'] = $this->endingcountdown->set($data['data_promo'][0]->end_date);
		$this->load->view('template/page', $data);

	}
	
	public function loadmore($type){
		$search = array(
			'query'	=> $this->input->get('q')
		);
		
		
		$this->input->get('category')!="" ? $category = json_decode($this->input->get('category'),TRUE) : $category = "all";
		if($this->input->get('rangeLow')!=""){
			$range['low']	= $this->input->get('rangeLow');
			$range['high']	= $this->input->get('rangeHigh');	
		} else $range="all";
		$sortby=$this->input->get('sortby');
		$sortby == "" ? $order = 'start_date' :	$order = $sortby;
		$page=$this->input->get('page');
		
		$search['queryExploded'] = explode(" ",$search['query']);
		
		if($type === "promo") {
			print_r($search);
			echo "/";
			print_r($category);
			echo "/";
			print_r($range);
			echo "/";
			print_r($order);
			echo "/";
			print_r($page);
			$search['result']['promo'] = $this->m_promo->search($search['queryExploded'],$category,$range,$order,$page);
			
			$data= array(
				'promo' 			=>	$this->discountformatter->promo($search['result']['promo']['result']['result']),
				'promoLimitedRows'	=>	$search['result']['promo']['result']['num_rows'],
				'promoRows'			=>	$search['result']['promo']['num_rows'],
				'query'				=>	$search['query'],
				'range'				=>	$range,
				'categorySelected' 	=>  $category,
				'sortby'			=>	$sortby,
			);
			
			if ($data['promoLimitedRows']) {
				foreach ($data['promo'] as $pr) {
					$pr->ending = $this->endingcountdown->set($pr->end_date);
				}
			}

			foreach ($data['promo'] as $d) { ?>
				<div class="promo">
						<div class="promo-detail">
							<div class="promo-store-logo">
								<a href="<?=base_url().$d->sslug ?>">
									<img src="<?= base_url() ?>assets/uploads/<?= $d->simage ?>" alt="">
								</a>
							</div>
						</div>
						<div class="promo-image">
							<a href="<?=base_url().$d->sslug.'/promo/'.$d->slug ?>">
								<img src="<?= base_url() ?>assets/uploads/<?= $d->image ?>" alt="">
							</a>
							<div class='promo-disc'>Up to <?= $d->discount ?>%</div>
						</div>
						<a href="<?=base_url().$d->sslug.'/promo/'.$d->slug ?>">
							<h3 class="promo-name">
								<?= $d->title ?>
							</h3>
							<h4>Promo akan berakhir <span><?= $d->ending ?></span> lagi</h4>
						</a>
							<?= $d->description ?>	
					</div>
			<?php }
		}else{
			$search['result']['product'] 	= $this->m_product->search($search['queryExploded'],$category,$range,$order,$page);

			$search['result']['product']['result']=$this->discountformatter->set($search['result']['product']['result']);
		
			$data= array(
				'product'			=>	$search['result']['product']['result']['result'],
				'productLimitedRows'=>	$search['result']['product']['result']['num_rows'],
				'productRows'		=>	$search['result']['product']['num_rows'],
				'query'				=>	$search['query'],
				'range'				=>	$range,
				'categorySelected' 	=>  $category,
				'sortby'			=>	$sortby,
			);

			foreach ($data['product'] as $d) { 
				$d->ending = $this->endingcountdown->set($d->pend_date);
				?>
				<div class="product">
					<div class="product-image">
						<a href="<?=base_url().$d->sslug.'/promo/'.$d->id ?>">
							<img src="<?= base_url() ?>assets/uploads/<?= $d->image ?>" alt="">
						</a>
						<div class='product-disc'>Up to <?= $d->discount ?>%</div>
					</div>
					<a href="<?=base_url().$d->sslug.'/promo/'.$d->id ?>">
						<h3 class="product-name">
							<?= $d->name ?>
						</h3>
						<h4>Promo akan berakhir <span><?= $d->ending ?></span> lagi</h4>
					</a>
						<?= $d->description ?>	
					<div class="product-detail">
						<div class="store-logo">
							<a href="<?=base_url().$d->sslug ?>">
								<img src="<?= base_url() ?>assets/uploads/<?= $d->simage ?>" alt="">
							</a>
						</div>
						<div class="product-price">
							<h5 class="old-price">Rp. <?= number_format($d->pricing['normal_price'],0,',','.') ?></h5>
							<h3 class="new-price">Rp. <?= number_format($d->pricing['new_price'],0,',','.') ?></h3>
							<p class="hemat">(hemat <?= number_format($d->pricing['diff'],0,',','.') ?>)</p>
						</div>
					</div>
				</div>
			<?php }
		}
	}


	
	public function autoComplete(){
		$q = $this->input->get('query');
		
		$result=$this->m_store->autoComplete($q);
		
		foreach ($result['result'] as $r) {
			echo "<a href='".base_urL().$r->slug."'>".$r->name."</a>";
		}
	}
	
}