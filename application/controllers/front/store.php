<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('m_store');
		$this->load->model('m_promo');
		$this->load->model('m_product');
		$this->load->library('googlemaps');
		$this->load->library('endingcountdown');
		$this->load->library('discountformatter');
	}

	public function index($slug) {
		$data = array(
			'title'        => 'Promodia',
			'css'          => 'style-store.css',
			'data_store'   => $this->m_store->getStore('slug', $slug, 'id', '1', '0'),
			'content'      => 'content/store',
            'plugins_css'  => array(
                'assets/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5',
                'assets/plugins/owl-carousel/owl-carousel/owl.carousel.css',
                'assets/plugins/owl-carousel/owl-carousel/owl.theme.css'
            ),
            'plugins_js'   => array(
                'assets/plugins/fancybox/source/jquery.fancybox.js?v=2.1.5',
                'assets/plugins/fancybox/script.js',
                'assets/plugins/owl-carousel/owl-carousel/owl.carousel.min.js'
            )
		);
		$data['data_promo'] = $this->discountformatter->promo($this->m_promo->getPromo('id_store', $data['data_store'][0]->id, 'title', '100', '0'));
		$data['num_promo'] = sizeof($data['data_promo']);
		$data['num_product'] = $this->m_product->getProduct('sslug', $slug)['num_rows'];
		if($data['num_promo']>0) {
            $data['ending'] = $this->endingcountdown->set($data['data_promo'][0]->end_date);
        }
		$this->googlemaps->bikin_peta($data['data_store'][0]->latitude, $data['data_store'][0]->longitude, 15);
		$data['map'] = $this->googlemaps->create_map();
		foreach ($data['data_promo'] as $promo){
			$promo->product= $this->discountformatter->set($this->m_product->getProduct('pid',$promo->id));
		}
		foreach ($data['data_store'] as $p) {
			$data['meta'] = array(
				'description'   => 'Klik untuk melihat promo dari '.$p->name.' sebelum terlambat!',
				'og:description'=> 'Klik untuk melihat promo dari '.$p->name.' sebelum terlambat!',
				'og:image'      => base_url().'assets/uploads/'.$p->image,
				'og:title'      => 'Promo dari '.$p->name,
				'og:url'        => base_url().$p->slug,
				'og:site_name'  => 'Promodia',
				'og:type'       => 'website',
				'twitter:card'  => 'photo',
				'twitter:url'   => base_url().$p->slug,
				'twitter:title' => $p->name,
				'twitter:description' => strip_tags($p->description),
				'twitter:image' => base_url().'assets/uploads/'.$p->image
			);
		}
		// print_r($data['data_promo']);
		$this->load->view('template/page', $data);
	}
}