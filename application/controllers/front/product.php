<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('m_product');
		$this->load->model('m_promo');
		$this->load->library('googlemaps');
		$this->load->library('endingcountdown');
		$this->load->library('discountformatter');
	}

	public function index($store='',$id='') {
		$data_product = $this->m_product->getProduct('product_id',$id);
		$data = array(
			'title'   		=> 'Promodia',
			'css'	  		=> 'style-product.css',
			'content' 		=> 'content/product',
			'data_product'	=> $this->discountformatter->set($data_product),
			'plugins_js'	=> array('assets/plugins/isotope/isotope.min.js',
				'assets/plugins/fancybox/source/jquery.fancybox.js?v=2.1.5',
                'assets/plugins/fancybox/script.js'
            ),
			'plugins_css'  => array(
	            'assets/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5'
            )
		);

		//jika nama store tidak sesuai dengan id product
		if ($data['data_product']['result'][0]->sslug != $store) {
			show_404();
		}

		$data['data_product_on_promo'] = $this->m_product->getProduct('promo_slug', $data['data_product']['result'][0]->pslug);

		$this->googlemaps->bikin_peta($data['data_product']['result'][0]->slatitude, $data['data_product']['result'][0]->slongitude, 15);
		$data['map'] = $this->googlemaps->create_map();

        $tag_data = explode(',',$data['data_product']['result'][0]->tag);
        $data['related'] = $this->m_product->related($tag_data,$data['data_product']['result'][0]->id,$data['data_product']['result'][0]->scategory_id);
        $data['ending'] = $this->endingcountdown->set($data['data_product']['result'][0]->pend_date);
		
		foreach ($data['data_product']['result'] as $p) {
			$data['meta'] = array(
				'description'   => strip_tags($p->description),
				'og:description'=> 'Klik untuk melihat promo dari '.$p->sname.' sebelum terlambat!',
				'og:image'      => base_url().'assets/uploads/'.$p->image,
				'og:title'      => $p->name,
				'og:url'        => base_url().$p->sslug.'/product/'.$p->id,
				'og:site_name'  => 'Promodia',
				'og:type'       => 'website',
				'twitter:card'  => 'photo',
				'twitter:url'   => base_url().$p->sslug.'/product/'.$p->id,
				'twitter:title' => $p->name,
				'twitter:description' => strip_tags($p->description),
				'twitter:image' => base_url().'assets/uploads/'.$p->image
			);
		}
		
		$this->load->view('template/page', $data);
	}
	
}