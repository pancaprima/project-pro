<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('m_promo');
		$this->load->model('m_product');
		$this->load->model('m_store');
		$this->load->model('m_category');
		$this->load->model('m_slider');
	}

	public function index() {
		$data = array(
			'title'      => 'Promodia',
			'css'        => 'style-home.css',
			'content'    => 'content/home',
			'plugins_js' => array('assets/plugins/isotope/isotope.min.js','assets/plugins/bxslider/jquery.bxslider.js'),
			'plugins_css'=> array('assets/css/jquery.bxslider.css'),
			'data'       => $this->m_category->getAll(),
			'slider'	 => $this->m_slider->getAll('page','Home'),
			'meta'		 => array(
				'description'   => 'Tempat dimana kamu dapat menemukan promo-promo menarik dari toko-toko favoritmu! Klik untuk melihat promo terbaik bulan ini, sebelum terlambat!',
				'og:description'=> 'Tempat dimana kamu dapat menemukan promo-promo menarik dari toko-toko favoritmu! Klik untuk melihat promo terbaik bulan ini, sebelum terlambat!',
				'og:image'      => base_url().'assets/images/banner.jpg',
				'og:title'      => 'Promodia',
				'og:url'        => base_url(),
				'og:site_name'  => 'Promodia',
				'og:type'       => 'website',
				'twitter:card'  => 'photo',
				'twitter:url'   => base_url(),
				'twitter:title' => 'Promodia',
				'twitter:description' => 'Tempat dimana kamu dapat menemukan promo-promo menarik dari toko-toko favoritmu! Klik untuk melihat promo terbaik bulan ini, sebelum terlambat!',
				'twitter:image' => base_url().'assets/images/banner.jpg'
			)
		);
		foreach ($data['data'] as $d) {
			$d->promo = $this->m_promo->getPromo('category_id',$d->id,'start_date',20,0);
			$d->store = $this->m_store->getStore('category_id',$d->id,'level',8,0);
			foreach ($d->promo as $p) {
				$p->product = $this->m_product->getProduct('promo_slug', $p->slug);
			}
		}
		// print_r($data['data']);
		$this->load->view('template/page', $data);
	}

}