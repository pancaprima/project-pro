<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class endingcountdown {

	function set($end) {
	    $sekarang = strtotime(date('Y-m-d H:i:s'));
	    $berakhir = strtotime($end);
	    $selisih  = $berakhir-$sekarang;

	    $a = array( 24 * 60 * 60  =>  'Hari',
	                     60 * 60  =>  'Jam',
	                          60  =>  'Menit',
	                           1  =>  'Detik'
	                );
	    foreach ($a as $detik => $satuan)
	    {
	        $d = $selisih / $detik;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . $satuan;
	        }
	    }
    }
}