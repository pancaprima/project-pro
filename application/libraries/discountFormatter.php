<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class discountformatter {

	function set($data_product) {
		foreach ($data_product['result'] as $d) {
		    if($d->promo_type == 'Discount') {
				$diff  = ($d->normal_price * $d->discount)/100;
				$new_price = $d->normal_price - $diff;
				$d->pricing = array(
					'diff'         => $diff,
					'new_price'    => $new_price,
					'discount'     => $d->discount,
					'normal_price' => $d->normal_price,
					'display'      => "<h3 class='product-disc'>".$d->discount."%</h3>"
				);
			} else if ($d->promo_type == 'New Price') {
				$diff  = $d->normal_price - $d->promo_factor;
				$new_price = $d->normal_price - $diff;
				$d->pricing = array(
					'diff'         => $diff,
					'new_price'    => $new_price,
					'discount'     => $d->discount,
					'normal_price' => $d->normal_price,
					'display'      => "<h3 class='product-disc'>".$d->discount."%</h3>"
				);
			} else if ($d->promo_type == 'Buy n Get n') {
				$factor = explode(',',$d->promo_factor);
				$d->normal_price = $d->normal_price*$factor[1];
				$diff  = $d->normal_price - (($factor[0]/$factor[1])*$d->normal_price);
				$new_price = $d->normal_price - $diff;
				$d->pricing = array(
					'diff'         => $diff,
					'new_price'    => $new_price,
					'discount'     => $d->discount,
					'normal_price' => $d->normal_price,
					'display'      => "<h3 class='product-disc extended'>Buy ".$factor[0]."<br/>Get ".$factor[1]."</h3>"
				);
			} else if ($d->promo_type == 'Free Stuff') {
				$diff  = $d->normal_price - $d->promo_factor;
				$new_price = $d->normal_price - $diff;
				$d->pricing = array(
					'diff'         => $diff,
					'new_price'    => $new_price,
					'discount'     => $d->discount,
					'normal_price' => $d->normal_price,
					'display'      => "<h3 class='product-disc extended'>Free<br/>Stuff</h3>"
				);
			}
		}
		return $data_product;
    }

    function promo($data_promo) {
    	foreach ($data_promo as $d) {
		    if($d->promo_type == 'Discount') {
				$d->pricing = array(
					'discount'     => $d->discount,
					'display'      => "<h3 class='product-disc'>".$d->discount."%</h3>"
				);
			} else if ($d->promo_type == 'New Price') {
				$d->pricing = array(
					'discount'     => $d->discount,
					'display'      => "<h3 class='product-disc'>".$d->discount."%</h3>"
				);
			} else if ($d->promo_type == 'Buy n Get n') {
				$factor = explode(',',$d->promo_factor);
				$d->pricing = array(
					'discount'     => $d->discount,
					'display'      => "<h3 class='product-disc extended'>Buy ".$factor[0]."<br/>Get ".$factor[1]."</h3>"
				);
			} else if ($d->promo_type == 'Free Stuff') {
				$d->pricing = array(
					'discount'     => $d->discount,
					'display'      => "<h3 class='product-disc extended'>Free<br/>Stuff</h3>"
				);
			}
		}
		return $data_promo;
    }
}