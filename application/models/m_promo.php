<?php

Class m_promo extends CI_Model {
    
    function insert($data){
        $this->db->insert('promo',$data);
    }

    function getAll($param='',$value=''){
        if($param=='store')  $this->db->where('id_store', $value);
        $this->db->select('*');
        $this->db->from('promo');
        $q  = $this->db->get();
        return $q->result();
    }

    function getDetail($id){
        $this->db->select('*');
        $this->db->from('promo');
        $this->db->where('id',$id);
        $q  = $this->db->get();
        return $q->result();
    }

    function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update('promo',$data);
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('promo');
    }
    
    /*Model untuk front*/
    public $promoJoin = "
            p.id,
            p.id_store,
            p.title,
            p.level,
            p.promo_type,
            p.discount,
            p.promo_factor,
            p.start_date,
            p.end_date,
            p.description,
            p.image,
            p.image2,
            p.slug,
            p.created,
            p.updated,
            s.id as sid,
            s.name as sname,
            s.category_id as scategory_id,
            s.level as slevel,
            s.address as saddress,
            s.longitude as slongitude,
            s.latitude as slatitude,
            s.description as sdescription,
            s.slug as sslug,
            s.image as simage,
            s.created as screated,
            s.updated as supdated
        ";
    
    function getPromo($param,$value,$order,$limit, $start){
        $this->db->select($this->promoJoin);
        $this->db->from('promo as p');
        $this->db->where('end_date >', date('Y-m-d H:i:s'));
        if($param == 'category_id') {
            if($value!='all'){
                $this->db->where('category_id',$value);    
            }
        } else if ($param == 'id_store') {
            $this->db->where('s.id',$value);
        }
        if($order == 'ending') {
            $this->db->where('p.end_date >=', date('Y-m-d H:i:s'));
            $this->db->order_by('p.end_date','asc');
        } else {
            $this->db->order_by($order,'desc');
        }
        $this->db->limit($limit, $start);
        $this->db->join('store as s','s.id=p.id_store','left');
        $q  = $this->db->get();
        return $q->result(); 
    }

    function getPromoDetail($store,$promo) {
        $this->db->select($this->promoJoin);
        $this->db->from('promo as p');
        $this->db->join('store as s','s.id=p.id_store','left');
        $this->db->where('s.slug', $store);
        $this->db->where('p.slug', $promo);
        $q  = $this->db->get();
        return $q->result(); 
    }

    function getNumPromo($category_id) {
        $this->db->from('promo');
        $this->db->join('store','store.id=promo.id_store','left');
        if($category_id != 'all') {
            $this->db->where('category_id', $category_id);
        }
        $q = $this->db->get();
        return $q->num_rows();
    }
    
    function search($query, $category, $range, $order,$page){
        
        $return['result'] = $this->searchResult($query, $category, $range, $order,$page);
        $page==1 ? $return['num_rows']= $this->searchNumrows($query, $category, $range, $order): $return['num_rows']=false;
        return $return;
        
    }
    
    private function searchNumrows($query, $category, $range, $order){
        
        $this->searchPreparation($query, $category, $range, $order);
        return $this->db->get()->num_rows();
      
    }
    
    private function searchResult($query, $category, $range, $order,$page){
        
        $this->searchPreparation($query, $category, $range, $order);
        $limit=12; $offset=($page-1)*$limit;
        $this->db->limit($limit,$offset);
        $q=$this->db->get();
        
        return array(
            'result' => $q->result(),
            'num_rows' => $q->num_rows()
        );
      
    }
    
    
    private function searchPreparation($query, $category, $range, $order){
        $firstQuery = false;
        foreach ($query as $c){
            if(!$firstQuery) {
                $mixedQuery="`p`.`title` LIKE '%".$c."%'";
                $firstQuery=true;
            }else{
                $mixedQuery=$mixedQuery." OR `p`.`title` LIKE '%".$c."%'";
            }          
        }
        
        $this->db->select($this->promoJoin);
        $this->db->from('promo as p');
        $this->db->join('store as s','p.id_store=s.id','left');
        if($range!="all")  {   $this->db->where('p.discount >=',$range['low']);  $this->db->where('p.discount <=',$range['high']); }
        if($category!="all") $this->db->where_in('s.category_id',$category);
        $this->db->where("(".$mixedQuery.")");    
        if($order == 'ending') { 
            $this->db->where('p.end_date >=', date('Y-m-d H:i:s'));
            $this->db->order_by('p.end_date','asc');
        } else {
            $this->db->order_by($order,'desc');
        }
    }

}