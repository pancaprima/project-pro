<?php

Class m_store extends CI_Model {

        function insert($data){
            $this->db->insert('store',$data);
        }

        function getAll(){
            $this->db->select('*');
            $this->db->from('store');
            $q  = $this->db->get();
            return $q->result();
        }

        function getDetail($id){
            $this->db->where('id',$id);
            $this->db->select('*');
            $this->db->from('store');
            $q  = $this->db->get();
            return $q->result();
        }

        function update($id,$data){
            $this->db->where('id',$id);
            $this->db->update('store',$data);
        }

        function delete($id){
            $this->db->where('id',$id);
            $this->db->delete('store');
        }
        
        
        /*Model Store untuk Front*/

        function getStore($param, $value, $order, $limit, $start){
            if($param=="slug") $this->db->where('slug',$value);
            else if($param=='category_id') $this->db->where('category_id', $value);
            $this->db->from('store');    
            $this->db->order_by($order,'desc');
            $this->db->limit($limit, $start);
            $q  = $this->db->get();
            return $q->result(); 
        }
              
        function search($query){
            $this->db->select('*');
            $this->db->from('store');
            $this->db->where('name =',$query);
            $q  = $this->db->get();
            $num_rows = $q->num_rows();
            $num_rows > 1 ? $result=$q->result() : $result=$q->row();
            $result = $q->result();
            $return = array (
                'result'    =>  $result,
                'num_rows'  =>  $num_rows,
            );
            return $return;
        }
        
        function autoComplete($query){
            $this->db->select('*');
            $this->db->from('store');
            $this->db->like('name',$query);
            $q  = $this->db->get();
            $num_rows = $q->num_rows();
            $result = $q->result();
            $return = array (
                'result'    =>  $result,
                'num_rows'  =>  $num_rows,
            );
            return $return;
        }
}