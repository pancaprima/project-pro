<?php

Class m_product extends CI_Model {



    function insert($data){
        $this->db->insert('product',$data);
    }

    function getAll(){
        $this->db->select('*');
        $this->db->from('product');
        $q  = $this->db->get();
        return $q->result();
    }

    function getDetail($id){
        $this->db->where('id',$id);
        $this->db->select('*');
        $this->db->from('product');
        $q  = $this->db->get();
        return $q->result();
    }

    function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update('product',$data);
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('product');
    }

     function getBiggestDiscount($id_promo){
        $this->db->where('id_promo',$id_promo);
        $this->db->select_max('discount');
        $result = $this->db->get('product')->row();  
        return $result->discount;
    }
    
    //-------------FRONT PRODUCT---------------------------------
    
    public $allJoin = '
            pd.id,
            pd.id_promo,
            pd.name,
            pd.description,
            pd.promo_type,
            pd.discount,
            pd.normal_price,
            pd.promo_factor,
            pd.image,
            pd.tag,
            pd.created,
            pd.updated,
            p.id as pid,
            p.id_store as pid_store,
            p.title as ptitle,
            p.level as plevel,
            p.promo_type as ppromo_type,
            p.discount as pdiscount,
            p.promo_factor as ppromo_factor,
            p.start_date as pstart_date,
            p.end_date as pend_date,
            p.description as pdescription,
            p.image as pimage,
            p.slug as pslug,
            p.created as pcreated,
            p.updated as pupdated,
            s.id as sid,
            s.name as sname,
            s.category_id as scategory_id,
            s.level as slevel,
            s.address as saddress,
            s.longitude as slongitude,
            s.latitude as slatitude,
            s.description as sdescription,
            s.slug as sslug,
            s.image as simage,
            s.created as screated,
            s.updated as supdated
        ';
        
    function related($query,$id,$category){
        $firstQuery = false;
        foreach ($query as $c){
            if(!$firstQuery) {
                $mixedQuery="`pd`.`tag` LIKE '%".$c."%'";
                $firstQuery=true;
            }else{
                $mixedQuery=$mixedQuery." OR `pd`.`tag` LIKE '%".$c."%'";
            }          
        }
        
        $this->db->select($this->allJoin);
        $this->db->from('product as pd');
        $this->db->join('promo as p','pd.id_promo=p.id','left');
        $this->db->join('store as s','p.id_store=s.id','left');
        $this->db->where("(".$mixedQuery.")");
        $this->db->where('pd.id !=',$id);

        $q = $this->db->get();
        $result=$q->result();
        
        if($q->num_rows()<4){
            $not_in[0]=$id;
            foreach($result as $r){
                array_push($not_in,$r->id);
            }
            $other_related=$this->other_related($not_in,$category,(4-$q->num_rows()));
            foreach($other_related as $or){
                array_push($result,$or);    
            }
            
        }
        return $result;
    }
    
    function other_related($not_in,$category,$minus){
        $this->db->select($this->allJoin);
        $this->db->from('product as pd');
        $this->db->join('promo as p','pd.id_promo=p.id','left');
        $this->db->join('store as s','p.id_store=s.id','left');
    
        $this->db->where('s.category_id',$category);
        $this->db->where_not_in('pd.id',$not_in);
        $this->db->limit($minus);
        
        $q = $this->db->get();
        $result=$q->result();
        
        return $result;
        
    }
        
    function getProduct($param, $value){
        $this->db->select($this->allJoin);
        $this->db->from('product as pd');
        $this->db->join('promo as p','pd.id_promo=p.id','left');
        $this->db->join('store as s','p.id_store=s.id','left');
        if($param=="product_id") $this->db->where('pd.id',$value);
        else if($param=="promo_slug") $this->db->where('p.slug',$value);
        else if($param=="pid") $this->db->where('p.id',$value);
        else if($param=="sslug") $this->db->where('s.slug',$value);
        $q  = $this->db->get();
        $result = $q->result();
        $num_rows = $q->num_rows();
        $return = array (
            'result'    =>  $result,
            'num_rows'  =>  $num_rows,
        );
        return $return;
    }

    function getProductNum($param, $value) {
        $this->db->select($this->allJoin);
        $this->db->from('product as pd');
        $this->db->join('promo as p','pd.id_promo=p.id','left');
        $this->db->join('store as s','p.id_store=s.id','left');
        if($param=="product_id") $this->db->where('pd.id',$value);
        else if($param=="promo_slug") $this->db->where('p.slug',$value);
        $q  = $this->db->get();
        return $q->num_rows(); 
    }
    
    function search($query, $category, $range, $order,$page){
        
        $return['result'] = $this->searchResult($query, $category, $range, $order,$page);
        $page==1 ? $return['num_rows']= $this->searchNumrows($query, $category, $range, $order): $return['num_rows']=false;
        return $return;
    }
    
    private function searchNumrows($query, $category, $range, $order){
        
        $this->searchPreparation($query, $category, $range, $order);
        return $this->db->get()->num_rows();
      
    }
    
    private function searchResult($query, $category, $range, $order,$page){
        
        $this->searchPreparation($query, $category, $range, $order);
        $limit=12; $offset=($page-1)*$limit;
        $this->db->limit($limit,$offset);
        $q=$this->db->get();
        return array(
            'result' => $q->result(),
            'num_rows' => $q->num_rows()
        );
      
    }
    
    private function searchPreparation($query, $category, $range, $order){
        //creating query
        /*dibuat karena permasalahan pada hirarki dalam penggunaan AND dan OR pada active record*/
        $firstQuery = false;
        foreach ($query as $c){
            if(!$firstQuery) {
                $mixedQuery="`pd`.`name` LIKE '%".$c."%'";
                $firstQuery=true;
            }else{
                $mixedQuery=$mixedQuery." OR `pd`.`name` LIKE '%".$c."%'";
            }          
        } 

        $this->db->select($this->allJoin);
        $this->db->from('product as pd');
        $this->db->join('promo as p','pd.id_promo=p.id','left');
        $this->db->join('store as s','p.id_store=s.id','left');
        
        if($range!="all")  {   $this->db->where('pd.discount >=',$range['low']);  $this->db->where('pd.discount <=',$range['high']); }
        if($category!="all") $this->db->where_in('s.category_id',$category);
        $this->db->where("(".$mixedQuery.")");  
        if($order == 'ending') { 
            $this->db->where('p.end_date >=', date('Y-m-d H:i:s'));
            $this->db->order_by('p.end_date','asc');
        } else {
            $this->db->order_by($order,'desc');
        } 
    }
}