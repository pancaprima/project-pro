<?php

Class m_log_product extends CI_Model {

    function insert($data){
        $this->db->insert('log_product',$data);
    }

    function getAll(){
        $this->db->select('*');
        $this->db->from('log_product');
        $q  = $this->db->get();
        return $return = array (
                'result'    =>  $q->result(),
                'num_rows'  =>  $q->num_rows(),
               );
    }

    function getDetail($id){
        $this->db->where('id_product',$id);
        $this->db->select('*');
        $this->db->from('log_product');
        $q  = $this->db->get();
        return $return = array (
                'result'    =>  $q->result(),
                'num_rows'  =>  $q->num_rows(),
               );
    }


    function delete($id){
        $this->db->where('id_product',$id);
        $this->db->delete('log_product');
    }
}