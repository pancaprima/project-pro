<?php

Class m_slider extends CI_Model {


    function insert($data){
        $this->db->insert('slider',$data);
    }

    function getAll($param,$value){
        $this->db->select('*');
        $this->db->from('slider');
        if ($param == 'page') {
            $this->db->like($param,$value);
        }
        $q  = $this->db->get();
        return $q->result();
    }

    function getDetail($id){
        $this->db->where('id',$id);
        $this->db->select('*');
        $this->db->from('slider');
        $q  = $this->db->get();
        return $q->result();
    }

    function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update('slider',$data);
    }

    function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('slider');
    }

}