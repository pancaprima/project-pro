<?php

Class m_log_promo extends CI_Model {

    function insert($data){
        $this->db->insert('log_promo',$data);
    }

    function getAll(){
        $this->db->select('*');
        $this->db->from('log_promo');
        $q  = $this->db->get();
        return $return = array (
                'result'    =>  $q->result(),
                'num_rows'  =>  $q->num_rows(),
               );
    }

    function getDetail($id){
        $this->db->where('id_promo',$id);
        $this->db->select('*');
        $this->db->from('log_promo');
        $q  = $this->db->get();
        return $return = array (
                'result'    =>  $q->result(),
                'num_rows'  =>  $q->num_rows(),
               );
    }

    function delete($id){
        $this->db->where('id_promo',$id);
        $this->db->delete('log_promo');
    }
    
}