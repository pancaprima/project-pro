<?php

Class m_admin extends CI_Model {

    function login($username, $password) {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function getAll() {
        $this->db->select('*');
        $this->db->from('admin');
        $q = $this->db->get();
        return $q;
    }

    function add_user($data) {
        $this->db->insert('admin', $data);
    }

    function get_user($username) {
        $this->db->where('id', $username);
        $q = $this->db->get('admin');
        return $q->result();
    }

    function edit_user($data, $username) {
        $this->db->where('id', $username);
        $this->db->update('admin', $data);
    }

    function delete_user($username) {
        $this->db->where('id', $username);
        $this->db->delete('admin');
    }

}

?>
