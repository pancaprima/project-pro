<?php

Class m_log_store extends CI_Model {

    function insert($data){
        $this->db->insert('log_store',$data);
    }

    function getAll(){
        $this->db->select('*');
        $this->db->from('log_store');
        $q  = $this->db->get();
        return $return = array (
                'result'    =>  $q->result(),
                'num_rows'  =>  $q->num_rows(),
               );
    }

    function getDetail($id){
        $this->db->where('id_store',$id);
        $this->db->select('*');
        $this->db->from('log_store');
        $q  = $this->db->get();
        return $return = array (
                'result'    =>  $q->result(),
                'num_rows'  =>  $q->num_rows(),
               );
    }


    function delete($id){
        $this->db->where('id_store',$id);
        $this->db->delete('log_store');
    }
}