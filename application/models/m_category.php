<?php

Class m_category extends CI_Model {

    function insert($data){
        $this->db->insert('category',$data);
    }

    function getAll(){
        $this->db->select('*');
        $this->db->from('category');
        $q  = $this->db->get();
        return $q->result();
    }

    function getDetail($id){
        $this->db->where('id',$id);
        $this->db->select('*');
        $this->db->from('category');
        $q  = $this->db->get()->row();
        return $q;
    }

    function update($id,$data){
        $this->db->where('id',$id);
        $this->db->update('category',$data);
    }

    function delete($id){
        $this->db->where('id',$id);
        $this->db->delete('category');
    }

    function getCategoryName($id) {
        $this->db->from('category');
        $this->db->where('id', $id);
        return $this->db->get()->row()->name;
    }
}

