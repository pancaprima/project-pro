<nav>
    <div class='container'>
        <div class="col-3 logo">
        	<!-- <h1 style='font-family:SourceSansPro-Light; color: #0e3b50; line-height: 60px; font-size: 26pt;'>
	            Promodia
	        </h1> -->
	        <a href="<?= base_url() ?>">
	            <img src="<?= base_url() ?>assets/images/logo1.png" />
	        </a>
        </div>
        <div class="search-bar col-6">
            <form id="pencarian" action="<?= base_url() ?>search" method="get">
	        	<input id="nav-search" type="text" name='q' placeholder="Cari toko, promo atau produk" autocomplete="off" data-base-url="<?= base_url() ?>"/>
	        	<div class="suggestion">
	        		<h4>Apakah yang Anda maksud:</h4>
	        		<div></div>
	        		<p>atau tekan Enter untuk hasil pencarian promo/produk dengan kata kunci '<span></span>'</p>
	        	</div>
	        	<button type="submit" id="cari">
					<h4>Search</h4>
	        	</button>
            </form>
	        <p class='nav-links'>
	        	<span>Telusuri promo: </span>
	        	<a href="<?= base_url() ?>all">Semua Kategori</a>
	        	<a href="<?= base_url() ?>elektronik">Elektronik</a>
	        	<a href="<?= base_url() ?>fashion">Fashion</a>
	        	<a href="<?= base_url() ?>kuliner">Kuliner</a>
	        </p>
        </div>
        <div class="col-3">
        	<div class='medsos-wrapper'>
	        	<div class='medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/googleplus.png" alt="">
	        	</div>
	        	<div class='medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/youtube.png" alt="">
	        	</div>
	        	<div class='medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/twitter.png" alt="">
	        	</div>
	        	<div class='medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/facebook.png" alt="">
	        	</div>
	        </div>
        </div>
    </div>
</nav>
<script>
	$('#nav-search').on('focus', function() {
		$('nav').addClass('expand');
	});
	$('#nav-search').on('blur', function(e) {
		$('nav').removeClass('expand');
		$('.suggestion').delay(200).hide(0);
	});
	
	$('#nav-search').on('keyup focus', function() {
		var base_url = $(this).attr('data-base-url');
		var query = $(this).val();
		
		if(query.length<2){
			$('#cari').prop("disabled", true);
		}else{
			$('#cari').prop("disabled", false);
			$.ajax({
				url: base_url+'search/autoComplete',
				type: 'get',
				context: this,
				data: {
					query: query
				},
				success: function(html) {
					if (html != '' && query.length > 2) {
						$('.suggestion div').html(html);
						$('.suggestion p span').html(query);
						$('.suggestion').show();
					} else if (html == '' || query.length < 3) {
						$('.suggestion').hide();
					}
				},
				error: function() {
					console.log('error');
				}
			})
		}
		
	});
</script>