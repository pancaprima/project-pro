<footer style='background-image:url(<?= base_url() ?>assets/images/footer.jpg)'>
	<div class="container">
		<div class="col-3 footer-logo">
			<img src="<?= base_url() ?>assets/images/logo2.png"/>
		</div>
		<div class="col-6">
			<p><b>Promodia.id</b> merupakan tempat berkumpulnya promo dari banyak brand dan penyedia jasa di Indonesia. Mempermudah para pemilik brand dalam mengenalkan promo mereka serta memudahkan masyarakat menemukan promo terbaru. punya promo untuk usaha kamu? pasang promonya sekarang! Ayo cari promo terbaru! <b>Gratis!</b>
			</div>
		<div class="col-3">
			<div class='medsos-wrapper'>
	        	<div class='medsos foot-medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/googleplus-white.png" alt="">
	        	</div>
	        	<div class='medsos foot-medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/youtube-white.png" alt="">
	        	</div>
	        	<div class='medsos foot-medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/twitter-white.png" alt="">
	        	</div>
	        	<div class='medsos foot-medsos'>
	        		<img src="<?=base_url();?>assets/images/icon/facebook-white.png" alt="">
	        	</div>
	        </div>
		</div>
	</div>
</footer>