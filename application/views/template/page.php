<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('template/head'); ?>
</head>
<body>
    <?php $this->load->view('template/nav'); ?>
    <main>
        <?php
        if (isset($content)) {
                $this->load->view($content);
        }
        ?>
    </main>
    <?php $this->load->view('template/footer'); ?>
</body>
</html>