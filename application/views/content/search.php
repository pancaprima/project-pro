<?php 
	$pro = array('p1.jpg','p2.jpg','p3.jpg','p4.jpg','p5.jpg');
?>
<div class="container">
	<div class="row">
		<div class="col-2" id="kiri">
			<div class="search-form">
				<h3 style="background-image:url(<?= base_url() ?>assets/images/bar.jpg)">Advanced Search</h3>
				<form class="form" action="<?= base_url() ?>search" method="get" id="advanceSearch">
				<input id="qInput" type="text" name="q" placeholder="Kata kunci" value="<?= $data['query'] ?>" >
				<div class="category-select">
					<p>Kategori</p>
					<?php
					foreach($data['category'] as $c){ ?>
						<input type='checkbox' name='category[<?=  $c->id  ?>]' id='c<?= $c->id ?>' value='<?= $c->id ?>'
						 	<?php 	if($data['categorySelected'] !="all") { 
								 		if(array_key_exists($c->id, $data['categorySelected'])) echo " checked";
									} else echo " checked";
							?>
						>
						<label for='c<?= $c->id ?>'><?= $c->name ?></label>
					<?php }	?>
				</div>
				<div class="disc-range">
					<p>Range Diskon</p>
					<input type="text" id="range-diskon" readonly/>
					<div id="discount_range"></div>
				</div>
				<input type="hidden" name="rangeLow" value="<?= $data['range'] !="all" ? $data['range']['low'] : 0 ?>">
				<input type="hidden" name="rangeHigh" value="<?= $data['range']!="all" ? $data['range']['high'] : 100 ?>">
				<input type="hidden" name="sortby" value="<?= $data['sortby']!="" ? $data['sortby'] : "" ?>">
				<button id="cariAdvance" type="submit" ><i class="fa fa-search"></i> Cari</button>
				</form>
			</div>
		</div>
		<div class="col-10">
			<div class="result-bar" style="background-image:url(<?= base_url() ?>assets/images/bar.jpg)">
				<h3><b>Hasil Pencarian : </b>
					<?php
					if($data['promoRows']>0){
						echo"
						<input type='radio' value='promo' name='search' id='promo' checked>
						<label for='promo'>".$data['promoRows']." promo</label>
						";
					}
					
					if($data['productRows']>0){
						echo"
						<input type='radio' value='product' name='search' id='product'";
						if($data['promoRows']==0) echo " checked";
						echo">
						<label for='product'>".$data['productRows']." produk</label>
						";
					}
					?>
					untuk pencarian <i><?= $data['query'] ?></i>
				</h3>
				<div class="sort-by">
					<h4>sort by :</h4>
					<select name="sortby" id="sortby">
						<option value="new" <?= $data['sortby'] == ""|| $data['sortby'] == "start_date" ? 'selected' : ''?>>Terbaru</option>
						<option value="discount" <?= $data['sortby'] == "discount" ? 'selected' : ''?>>Diskon Terbesar</option>
						<option value="ending" <?= $data['sortby'] == "ending" ? 'selected' : ''?>>Ending Soon</option>
					</select>
				</div>
				<div class="display-selector">
					<input type="radio" value="grid" name="display" id="grid" checked>
					<label for="grid"><i class="fa fa-th"></i> Grid</label>
					<input type="radio" value="list" name="display" id="list">
					<label for="list"><i class="fa fa-bars"></i> List</label>
				</div>
			</div>
			<?php if(!$data['promo'] && !$data['product']){
				echo "
				<div class='no-result'>
					<h1><i class='fa fa-search'></i> Hasil Tidak Ditemukan</h1>
				</div>
				";
			}else{?>
			<div class="result-gallery">
				<div class="promoResult">
					
				<?php foreach($data['promo'] as $pd){ ?>
					<div class="promo">
						<div class="promo-detail">
							<div class="promo-store-logo">
								<a href="<?=base_url().$pd->sslug ?>">
									<img src="<?= base_url() ?>assets/uploads/<?= $pd->simage ?>" alt="">
								</a>
							</div>
						</div>
						<div class="promo-image">
							<a href="<?=base_url().$pd->sslug.'/promo/'.$pd->slug ?>">
								<img src="<?= base_url() ?>assets/uploads/<?= $pd->image ?>" alt="">
							</a>
							<div class='promo-disc'>Up to <?= $pd->discount ?>%</div>
						</div>
						<a href="<?=base_url().$pd->sslug.'/promo/'.$pd->slug ?>">
							<h3 class="promo-name">
								<?= $pd->title ?>
							</h3>
							<h4>Promo akan berakhir <span><?= $pd->ending ?></span> lagi</h4>
						</a>
							<?= $pd->description ?>	
					</div>
				<?php }	?>
					<?php if($data['promoRows'] > 12) { ?>
					<div class="result-paging">
						<h4 class="load-more" data-page='2' data-type='promo'>Load More</h4>
					</div>
					<?php } ?>
				</div>
				<div class="productResult <?= $data['promoRows'] > 0 ? " hidden" : "" ?>">
				<?php foreach($data['product'] as $pd){ ?>
					<div class="product">
						<div class="product-image">
							<a href="<?=base_url().$pd->sslug.'/product/'.$pd->id ?>">
								<img src="<?= base_url() ?>assets/uploads/<?= $pd->image ?>" alt="">
							</a>
							<?=	$pd->pricing['display']	?>
						</div>
						<a href="<?=base_url().$pd->sslug.'/product/'.$pd->id ?>">
							<h3 class="product-name">
								<?= $pd->name ?>
							</h3>
						</a>
							<?= $pd->description ?>
						<div class="product-detail">
							<div class="store-logo">
								<a href="<?=base_url().$pd->sslug ?>">
									<img src="<?= base_url() ?>assets/uploads/<?= $pd->simage ?>" alt="">
								</a>
							</div>
						<div class="product-price">
							<h5 class="old-price">Rp. <?= number_format($pd->pricing['normal_price'],0,',','.') ?></h5>
							<h3 class="new-price">Rp. <?= number_format($pd->pricing['new_price'],0,',','.') ?></h3>
							<p class="hemat">(hemat <?= number_format($pd->pricing['diff'],0,',','.') ?>)</p>
						</div>
						</div>
					</div>
				<?php }	?>
					<?php if($data['productRows'] > 12 ) { ?>
					<div class="result-paging">
						<h4 class="load-more" data-page='2' data-type='product'>Load More</h4>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
			
		</div>
	</div>
</div>
<script>
	$('#discount_range').slider({
		range: true,
		min: 0,
		max: 100,
		step: 5,
		values: [<?= $data['range'] !="all" ? $data['range']['low'] : 0 ?> , <?= $data['range'] !="all" ? $data['range']['high'] : 100 ?>],
		slide: function(event, ui) {
			$('#range-diskon').val(ui.values[ 0 ] + "% - " + ui.values[ 1 ] + "%" );
			$('input[name="rangeLow"]').val(ui.values[ 0 ]);
			$('input[name="rangeHigh"]').val(ui.values[ 1 ]);
		}
	});
	$("#range-diskon").val($("#discount_range").slider("values", 0)+"% -  " + $("#discount_range").slider("values", 1)+"%");
	$("#kiri").stick_in_parent({
		offset_top: 100
	});
	$('input[name=display]').on('change', function() {
		var isi = $(this).val();
		if(isi == "list") {
			$('.result-gallery').addClass('list');
		} else {
			$('.result-gallery').removeClass('list');
		}
	});
	
	$(document).ready(function(){
		
		$('input:radio[name=search]').on('change', function() {
			var checked = $(this).val();
			$('.promoResult').addClass('hidden');
			$('.productResult').addClass('hidden');
			if(checked == "product") {
				$('.productResult').removeClass('hidden');
				console.log(checked);
			} else {
				$('.promoResult').removeClass('hidden');
				console.log(checked);
			}
		});
		
	});
	
	
	$('#sortby').on('change',function(){
		$('input[name="sortby"]').val($(this).val());
		$('#advanceSearch').submit();
	});
	
	$('.load-more').on('click', function() {
		var promoRows = <?= $data['promoRows'] ?>;
		var productRows = <?= $data['productRows'] ?>;
		var promoPageNum = Math.ceil(parseInt(promoRows)/12);
		var productPageNum = Math.ceil(parseInt(productRows)/12);
		var base_url = '<?= base_url() ?>';
		var type = $(this).attr('data-type');
		var category = '<?= json_encode($data["categorySelected"], JSON_FORCE_OBJECT) ?>';
		var sortby = '<?= $data["sortby"] ?>';
		var rangeLow = $('input[name="rangeLow"]').val();
		var rangeHigh = $('input[name="rangeHigh"]').val();
		var page = $(this).attr('data-page');
		$.ajax({
			url: base_url+'search/loadmore/'+type,
			context: this,
			type: 'get',
			data: {
				page: page,
				category: category,
				sortby: sortby,
				rangeLow: rangeLow,
				rangeHigh: rangeHigh
			},
			beforeSend: function() {

			},
			success: function(html) {
				$(this).parent().before(html);
				var nextpage = parseInt(page) + 1;
				if ((type == 'promo') && (page == promoPageNum)) {
					$('.promoResult').find('.result-paging').hide();
				} else if ((type == 'product') && (page == productPageNum)) {
					$('.productResult').find('.result-paging').hide();
				} else {
					$(this).attr('data-page', nextpage);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			}
		});
	});

	$('#qInput').on('keyup focus', function() {
		query = $(this).val();
		
		if(query.length<2){
			$('#cariAdvance').prop("disabled", true);
		}else{
			$('#cariAdvance').prop("disabled", false);
		}
	});
	
</script>