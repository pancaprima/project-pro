<div class="container">
	<div class="row">
		<div class="col-9">
			<div class="slider">
				<ul class="bxslider">
					<?php 
					foreach ($slider as $s) { ?> 
						<?php if ($s->status == "active") { ?>
							<li><img src='<?= base_url() ?>assets/uploads/<?= $s->image ?> '/></li>
					<?php }} ?>
				</ul>
			</div>
		</div>
		<div class="col-3">
		</div>
	</div>
	<?php if ($category != 'Semua Promo') { ?>
	<div class="store-display">
		<h5>Featured Store</h5>
		<?php foreach($data_store as $s) { ?>
		<div class="store-logo" style="background-image:url(<?= base_url() ?>assets/uploads/<?= $s->image ?>)">
		</div>
		<?php } ?>
	</div>
	<?php } ?>
	<div class="category">
		<div class="row">
			<div class="result-bar" style="background-image:url(<?= base_url() ?>assets/images/bar.jpg)">
				<h3><span><?= $category ?></span><p><?= $promo_count ?> items</p></h3>
				<div class="sort-by">
					<h4>sort by :</h4>
					<select name="sortby" id="sortby" data-category="<?= $category != 'Semua Promo' ? strtolower($category) : 'all' ?>">
						<option value="new" <?= $sortby == "" ? 'selected' : ''?>>Terbaru</option>
						<option value="discount" <?= $sortby == "discount" ? 'selected' : ''?>>Diskon Terbesar</option>
						<option value="ending" <?= $sortby == "ending" ? 'selected' : ''?>>Ending Soon</option>
					</select>
				</div>
			</div>
		</div>
		<div class="category-display row">
			<div id='product-display' class="product-display">
				<?php
				foreach ($data_promo as $d) { ?>
				<div class="product lvl-<?= $d->level; ?>" style='background-image:url(<?= base_url() ?>assets/uploads/<?= $d->image; ?>)'>
					<a href="<?= base_url().$d->sslug.'/promo/'.$d->slug;?>" class="anchor" title="Click to view this promo"></a>
					<div class="badge" style='background-image:url(<?= base_url() ?>assets/images/pft.jpg)'>
						<?php
						if ($d->promo_type == "Buy n Get n") {
							$factor = explode(',', $d->promo_factor);
							echo "<h3>Buy ".$factor[0]."<br/>Get ".$factor[1]."</h3>";
						} else if ($d->promo_type == "Free Stuff") {
							echo "<h3>Free<br/>Stuff</h3>";
						} else {
							echo "<h3>Up to<br/>".$d->discount."%</h3>";
						}
						?>
					</div>
					<div class="overlay">
						<div class="top-info">
							<!-- <p><i class="fa fa-clock-o"></i> 12 jam tersisa</p> -->
							<h3><?= $d->title; ?></h3>
							<h4>by <span><?= $d->sname; ?></span></h4>
						</div>
						<div class="small-info">
							<h3><?= $d->title; ?></h3>
							<h4>by <a href="<?= base_url().$d->sslug; ?>"><span><?= $d->sname; ?></span></a></h4>
						</div>
						<div class="bottom-info">
							<p><?= $d->description; ?></p>
						</div>
					</div>
				</div>
				<?php	
				}
				?>
			</div>
		</div>
	</div>
</div>
<script>
	$('#sortby').on('change', function() {
		var sortby   = $(this).val();
		var lokasi   = $(this).attr('data-category');
		var base_url = "<?= base_url() ?>";
		if (sortby != 'new') {
			window.location.href = base_url+lokasi+'?sortby='+sortby;
		} else {
			window.location.href = base_url+lokasi;
		}
	});
	$('.product-display').isotope({
		masonry: {
			columnWidth: 198,
			gutter: 2
		},
		itemSelector: '.product',
		percentPosition: false
	});
	  
    $('.bxslider').bxSlider({
        mode: 'fade',
        captions: true,
        auto: true,
        autoControls: false,
        pager: false
    });
</script>