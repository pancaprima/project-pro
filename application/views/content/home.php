<div class="container">
	<div class="row">
		<div class="col-9">
			<div class="slider">
				<ul class="bxslider">
					<?php 
					foreach ($slider as $s) { ?> 
						<?php if ($s->status == "active") { ?>
							<li><img src='<?= base_url() ?>assets/uploads/<?= $s->image ?> '/></li>
					<?php }} ?>
				</ul>
			</div>
		</div>
		<div class="col-3">
		</div>
	</div>
	<?php foreach ($data as $d) { ?>
	<div class="category">
		<div class="row">
			<div class="category-icon" style='background-image:url(<?= base_url() ?>assets/images/pft.jpg);'>
				<img src="<?= base_url() ?>assets/images/icon/<?= $d->image ?>" alt="">
			</div>
			<div class="category-head">
				<div class="category-label col-3" style='background-image:url(<?= base_url() ?>assets/images/tab.jpg)'>
					<h4><?= $d->name ?></h4>
				</div>
				<div class="category-sort">
					<a href="<?= base_url().strtolower($d->name); ?>">
						Terbaru
					</a>
					<a href="<?= base_url().strtolower($d->name)."?sortby=discount"; ?>">
						Diskon Tertinggi
					</a>
					<a href="<?= base_url().strtolower($d->name)."?sortby=ending"; ?>">
						Ending Soon
					</a>
				</div>
			</div>
		</div>
		<div class="category-display row">
			<div class="store-display">
				<?php foreach($d->store as $s) { ?>
				<div class="store-logo" style="background-image:url(<?= base_url() ?>assets/uploads/<?= $s->image ?>)">
					<a href="<?= base_url().$s->slug ?>"></a>
				</div>
				<?php } ?>
			</div>
			<div id='product-display' class="product-display">
				<?php
				foreach ($d->promo as $d) {
                    if($d->slevel==2 || $d->slevel==4){
                        if($d->image2!=null) {
                            $img = $d->image2;
                        }else{
                            $img=$d->image;
                        }
                    }else{
                        $img=$d->image;
                    }
                    ?>
				<div class="product lvl-<?= $d->slevel; ?>" style='background-image:url(<?= base_url() ?>assets/uploads/<?= $img; ?>)'>
					<a href="<?= base_url().$d->sslug.'/promo/'.$d->slug;?>" class="anchor" title="Click to view this promo"></a>
					<div class="badge" style='background-image:url(<?= base_url() ?>assets/images/pft.jpg)'>
						<?php
						if ($d->promo_type == "Buy n Get n") {
							$factor = explode(',', $d->promo_factor);
							echo "<h3>Buy ".$factor[0]."<br/>Get ".$factor[1]."</h3>";
						} else if ($d->promo_type == "Free Stuff") {
							echo "<h3>Free<br/>Stuff</h3>";
						} else {
							echo "<h3>Up to<br/>".$d->discount."%</h3>";
						}
						?>
					</div>
					<div class="overlay">
						<div class="top-info">
							<!-- <p><i class="fa fa-clock-o"></i> 12 jam tersisa</p> -->
							<h3><?= $d->title; ?></h3>
							<h4>by <span><?= $d->sname; ?></span></h4>
						</div>
						<div class="small-info">
							<h3><?= $d->title; ?></h3>
							<h4>by <a href="<?= base_url().$d->sslug; ?>"><span><?= $d->sname; ?></span></a></h4>
						</div>
						<div class="bottom-info">
							<p><?= $d->description; ?></p>
						</div>
					</div>
				</div>
				<?php	
				}
				?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<script>
	$('.product-display').isotope({
		masonry: {
			columnWidth: 198,
			gutter: 2
		},
		itemSelector: '.product',
		percentPosition: false
	});
	  
    $('.bxslider').bxSlider({
        mode: 'fade',
        captions: true,
        auto: true,
        autoControls: false,
        pager: false
    });
</script>