<?= $map['js']; ?>
<?php 
	foreach ($data_promo as $d) {
?>
<div class="container">
	<div class='row'>
		<div class='col-75'>
			<div class='banner-wrapper'>
				<div class="banner-left" style="background-image:url(<?= base_url() ?>assets/images/footer.jpg)">
					<?= $type == 'PO' ? '<div class="all-item">All<br/>Items</div>' : '' ?>
					<a class='fancybox' href="<?= base_url() ?>assets/uploads/<?= $d->image ?>" data-fancybox-group="gallery"><img class="image-wrapper" src="<?=base_url();?>assets/uploads/<?= $d->image ?>">
					</a>
					<h4>Berakhir <span><?= $ending ?></span> lagi.</h4>
				</div>
				<div class='banner-desc'>
					<h2 class='promo-title'><?= $d->title ?></h2>
					<h4 class='promo-desc'><?= $d->description ?></h4>
					<div class="banner-footer">
						<div class='view'><i class="fa fa-eye"></i> 250 views</div>
						<div class="sharer">
							<a class='fb-share' onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(base_url().$d->sslug."/promo/".$d->slug) ?>', '_blank', 'location=no,width=520,scrollbars=no,status=no'); return false;"><i class="fa fa-facebook"></i> Share</a>
							<a class='tw-share' onclick="window.open('https://twitter.com/intent/tweet?text=<?= $d->sname ?> lagi ada promo lho!&url=<?= urlencode(base_url().$d->sslug."/promo/".$d->slug) ?>&hashtags=promodia', '_blank', 'location=no,width=520,scrollbars=no,status=no'); return false;"><i class="fa fa-twitter"></i> Tweet</a>
						</div>
					</div>
				</div>
			</div>
			
			<?php if($data_product['num_rows'] != 0) { ?>
			<div class="result-bar" style="background-image:url(<?= base_url() ?>assets/images/bar.jpg)">
				<h3><?= $product_count ?> product(s) in this promo</h3>
				<div class="sort-by" style="display:none;">
					<h4>sort by :</h4>
					<select name="sortby" id="sortby">
						<option value="Terbaru">Terbaru</option>
						<option value="Diskon Terbesar">Diskon Terbesar</option>
						<option value="Ending Soon">Ending Soon</option>
					</select>
				</div>
				<div class="display-selector">
					<input type="radio" value="grid" name="display" id="grid" checked>
					<label for="grid"><i class="fa fa-th"></i> Grid</label>
					<input type="radio" value="list" name="display" id="list">
					<label for="list"><i class="fa fa-bars"></i> List</label>
				</div>
			</div>
			<div class="result-gallery">
				<?php foreach ($data_product['result'] as $p) { ?>
				<div class="product">
					<div class="product-image">
						<a href="<?= base_url().$p->sslug.'/product/'.$p->id ?>">
							<img src="<?= base_url() ?>assets/uploads/<?= $p->image ?>" alt="">
						</a>
					</div>
					<?= $p->pricing['display']; ?>
					<a href="<?= base_url().$p->sslug.'/product/'.$p->id ?>"><h3 class="product-name">
						<?= $p->name ?>
					</h3>
					</a>
					<?= $p->description ?>
					<div class="product-detail">
						<div class="product-price">
							<h5 class="old-price">Rp. <?= number_format($p->pricing['normal_price'],0,',','.') ?></h5>
							<h3 class="new-price">Rp. <?= number_format($p->pricing['new_price'],0,',','.') ?></h3>
							<p class="hemat">(hemat <?= number_format($p->pricing['diff'],0,',','.') ?>)</p>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="result-paging" style="display:none;">
				<div class="page-numbers">
					<span>1</span>
					<span class='active'>2</span>
					<span>3</span>
				</div>
				<div class="page-switch">
					<span class="prev inactive">Previous Page</span>
					<span class="next">Next Page</span>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class='col-45' id='kiri'>
			<div class='map-wrapper'>
				<div class='map'>
					<?= $map['html']; ?>
				</div>
				<div class='address'>
					<div class='row'>
						<div class='col-6'>
							<div class='address-name'>
								<a href="<?= base_url().$d->sslug ?>"><h3><?= $d->sname ?></h3></a>
							</div>
						</div>
						<div class='col-6'>
							<div class='logo-wrapper'>
								<img src="<?= base_url() ?>assets/uploads/<?= $d->simage ?>" alt="logo">
							</div>
						</div>
					</div>
					<div class='address-description'>
						<h5><?= $d->sdescription ?></h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script>
	// $('.result-gallery').not('list').isotope({
	// 	masonry: {
	// 		gutter: 10
	// 	},
	// 	itemSelector: '.product',
	// 	percentPosition: true
	// });
	$("#kiri").stick_in_parent({
		offset_top: 100
	});
	$('input[name=display]').on('change', function() {
		var isi = $(this).val();
		if(isi == "list") {
			$('.result-gallery').addClass('list');
		} else {
			$('.result-gallery').removeClass('list');
		}
	})
</script>

