<?= $map['js']; ?>
<?php foreach ($data_store as $i) { ?>
<div class="container">
	<div class="row">
		<div class='store'>
			<div class='map'>
				<div class="map-unlocker"></div>
				<?= $map['html']; ?>
			</div>
			<div class='col-45'>
				<div class='store-logo-profil'>
                    <a class="fancybox" href="<?= base_url() ?>assets/uploads/<?= $i->image ?>">
					    <img src="<?= base_url() ?>assets/uploads/<?= $i->image ?>" alt="" />
                    </a>
				</div>
			</div>
			<div class='col-75'>
				<div class='store-name'>
					<h2><?= $i->name ?></h2>
					<div class="statistic">
						<span><a><?= $num_promo ?></a> Promos</span>
						<span><a><?= $num_product ?></a> Products</span>
						<span><a>250</a> Views</span>
					</div>
					<div class="sharer">
						<a class='fb-share' onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(base_url().$i->slug) ?>', '_blank', 'location=no,width=520,scrollbars=no,status=no'); return false;"><i class="fa fa-facebook"></i> Share</a>
						<a class='tw-share' onclick="window.open('https://twitter.com/intent/tweet?text=Promo dari <?= $i->name ?>!&url=<?= urlencode(base_url().$i->slug) ?>&hashtags=promodia', '_blank', 'location=no,width=520,scrollbars=no,status=no'); return false;"><i class="fa fa-twitter"></i> Tweet</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='row store-detail'>
		<div class='col-45'>
			<div class='address'>
				<h3>Alamat</h3>
				<p><?= $i->address ?></p>
			</div>
		</div>
		<div class='col-75'>
			<div class='description'>
				<h3>Tentang</h3>
				<?= $i->description ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php foreach($data_promo as $dp) { ?>
		<?php if ($dp->product['num_rows'] == 0) { ?>
		<div class='row promo-group no-product'>
			<div class="result-gallery">
				<div class="promo-detail" style="background-image:url(<?=base_url()?>assets/images/footer.jpg);">
					<a class="fancybox" href="<?= base_url() ?>assets/uploads/<?= $dp->image ?>">
						<img src="<?= base_url() ?>assets/uploads/<?= $dp->image ?>" alt="">
					</a>
				</div>
				<div class="promo-desc">
					<div class="head">
						<h3><a href="<?= base_url().$dp->sslug.'/promo/'.$dp->slug ?>"><?= $dp->title ?></a></h3>
						<h4>Berakhir <span><?= $ending ?></span> lagi.</h4>
						<?= $dp->pricing['display'] ?>
					</div>
					<p><?= $dp->description ?></p>
				</div>
			</div>
		</div>
		<?php } else { ?>
		<div class='row promo-group'>
			<div class="result-gallery">
				<div class="promo-detail" style="background-image:url(<?=base_url()?>assets/images/footer.jpg);">
					<a class="fancybox" href="<?= base_url() ?>assets/uploads/<?= $dp->image ?>">
						<img src="<?= base_url() ?>assets/uploads/<?= $dp->image ?>" alt="">
					</a>
					<h3><a href="<?= base_url().$dp->sslug.'/promo/'.$dp->slug ?>"><?= $dp->title ?></a></h3>
					<p>(<?= $dp->product['num_rows'] ?> products)</p>
					<h4>Berakhir <span><?= $ending ?></span> lagi.</h4>
				</div>
				<div class="owl-carousel wrapper-product">
					<?php foreach ($dp->product['result'] as $dpp) { ?>
					<div class="product">
						<div class="product-image">
							<a href="<?= base_url().$dpp->sslug.'/product/'.$dpp->id ?>" >
							<img src="<?= base_url() ?>assets/uploads/<?= $dpp->image ?>" alt="">
							</a>
						</div>
						<a href="<?= base_url().$dpp->sslug.'/product/'.$dpp->id ?>" >
							<h3 class="product-name">
								<?= $dpp->name ?>
							</h3>
						</a>
						<?= $dpp->description ?>
						<div class="product-detail">
							<?= $dpp->pricing['display'] ?>
							<div class="product-price">
								<h5 class="old-price">Rp. <?= number_format($dpp->pricing['normal_price'],0,',','.') ?></h5>
								<h3 class="new-price">Rp. <?= number_format($dpp->pricing['new_price'],0,',','.') ?></h3>
								<p class="hemat">(Hemat <?= number_format($dpp->pricing['diff'],0,',','.') ?>)</p>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="products-footer">
					<p>Displaying <?= $dp->product['num_rows'] >= 8 ? '8' : $dp->product['num_rows'] ?> of <?= $dp->product['num_rows'] ?> <a href="<?= base_url().$dp->slug ?>"><?= $dp->product['num_rows'] > 8 ? 'View All' : 'View Promo Page' ?> <i class="fa fa-angle-double-right"></i></a></p>
				</div>
			</div>
		</div>
		<?php } ?>
	<?php } ?>
</div>
<script>
	$(".wrapper-product").owlCarousel({
		items: 4,
		autoPlay: true
	});
	$('.map-unlocker').on('click', function() {
		$(this).css('display','none');
	})
	$('#discount_range').slider({
		range: true,
		min: 0,
		max: 100,
		step: 5,
		values: [0, 100],
		slide: function(event, ui) {
			$('#range-diskon').val(ui.values[ 0 ] + "% - " + ui.values[ 1 ] + "%" );
		}
	});
	$("#range-diskon").val($("#discount_range").slider("values", 0)+"% -  " + $("#discount_range").slider("values", 1)+"%");
	$("#kiri").stick_in_parent({
		offset_top: 100
	});
	$('input[name=display]').on('change', function() {
		var isi = $(this).val();
		if(isi == "list") {
			$('.result-gallery').addClass('list');
		} else {
			$('.result-gallery').removeClass('list');
		}
	});
</script>