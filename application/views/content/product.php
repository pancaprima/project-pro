<?= $map['js']; ?>
<div class="container">
	<div class="row">
		<div class="col-8">
			<?php foreach($data_product['result'] as $d){
				?>
			
			<div class="wrapper-detail">
				<?= $d->pricing['display']; ?>
				<div class="image-product">
                    <a class="fancybox" href="<?= base_url() ?>assets/uploads/<?= $d->image ?>" data-fancybox-group="gallery">
	                    <img src="<?= base_url()."assets/uploads/".$d->image ?>">
	                </a>
                </div>
				<div class="wrapper-price">
					<div class="promo-name">
						<h1><?= $d->name ?></h1>
						<h3><a href="<?= base_url().$d->sslug.'/promo/'.$d->pslug ?>"><?= $d->ptitle ?></a> by <a href="<?= base_url().$d->sslug; ?>"><?= $d->sname ?></a>
						</h3>
					</div>
					<div class="price">
						<a class="old-price">Rp. <?= number_format($d->pricing['normal_price'],0,',','.') ?></a><br>
						<h1>Rp. <?= number_format($d->pricing['new_price'],0,',','.') ?></h1>
						<a class="dif">(hemat <?= number_format($d->pricing['diff'],0,',','.') ?>)</a>
					</div>
					<div class="share">
						<i class="fa fa-clock-o"> </i> promo akan berakhir <span><?= $ending; ?></span> lagi
						<div class="sharer">
							<a class='fb-share' onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(base_url().$d->sslug."/product/".$d->id) ?>', '_blank', 'location=no,width=520,scrollbars=no,status=no'); return false;"><i class="fa fa-facebook"></i> Share</a>
							<a class='tw-share' onclick="window.open('https://twitter.com/intent/tweet?text=<?= $d->sname ?> lagi ada promo lho!&url=<?= urlencode(base_url().$d->sslug."/product/".$d->id) ?>&hashtags=promodia', '_blank', 'location=no,width=520,scrollbars=no,status=no'); return false;"><i class="fa fa-twitter"></i> Tweet</a>
						</div>
					</div>
				</div>
			</div>
			<div class="description">
				<h1  style="background-image:url(<?= base_url() ?>assets/images/bar.jpg)">Product Description</h1>
				<?= $d->description ?>
			</div>
		</div>
		<div class="col-4">
			<div class='map-wrapper'>
				<div class='map'>
					<?= $map['html']; ?>
				</div>
				<div class='address'>
					<div class='row'>
						<div class='col-6'>
							<div class='address-name'>
								<a href="<?= base_url().$d->sslug ?>"><h3><?= $d->sname ?></h3></a>
							</div>
						</div>
						<div class='col-6'>
							<div class='logo-wrapper'>
								<img src="<?= base_url() ?>assets/uploads/<?= $d->simage ?>" alt="logo">
							</div>
						</div>
					</div>
					<div class='address-description'>
						<h5><?= $d->sdescription ?></h5>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
		if($data_product_on_promo['num_rows'] > 1) {
			
		?>
	<div class="row">
		<div class="col-12">
			<div class="wrapper-promo">
				<div class="image-promo"><img src="<?= base_url() ?>assets/uploads/<?= $d->pimage ?>" alt=""></div>
				<div class="wrapper-other-promo">
					<h2>Juga dalam promo ini</h2>
					<?php foreach ($data_product_on_promo['result'] as $dp) { 
						if($dp->id != $d->id) { ?>
							<a href="<?= base_url().$dp->sslug.'/product/'.$dp->id ?>">
								<div class="other-promo">
									<div class="other-discount">-50%</div><img src="<?= base_url() ?>assets/uploads/<?= $dp->image ?>">
								</div>
							</a>
					<?php 	}
						} 
					?>
				</div>
			</div>
		</div>
	</div>

	<?php } ?>
		<div class="wrapper-product">
			<div class="row">
				<div class="col-12">
			<div class="result-bar" style="background-image:url(<?= base_url() ?>assets/images/bar.jpg)">
				<h3 style='font-weight:bold; font-size:16pt;'>RELATED PRODUCT</h3>
			</div>
			<div class="result-gallery">
            <?php foreach($related as $rel){ ?>
                <?php
                if($rel->promo_type == 'Discount'){
                    $rel->potongan = ($rel->normal_price * $rel->discount)/100;
                    $rel->new_price = ($rel->normal_price - $rel->potongan);
                    $discount = $rel->discount;
                    $disc= '<h3 class="product-disc">'.$discount.'%</h3>';
                } else if($rel->promo_type == 'New Price'){
                    $rel->potongan = $rel->normal_price - $rel->promo_factor;
                    $rel->new_price = $rel->normal_price - $rel->potongan;
                    $discount = $rel->discount;
                    $disc= '<h3 class="product-disc">'.$discount.'%</h3>';
                } else if($rel->promo_type == 'Buy n Get n'){
                    $factor = explode(',',$rel->promo_factor);
                    $rel->normal_price = $rel->normal_price * $factor[1];
                    $rel->potongan = $rel->normal_price - (($factor[0]/$factor[1]) * $rel->normal_price);
                    $rel->new_price = $rel->normal_price - $rel->potongan;
                    $discount="Buy ".$factor[0]."<br/>Get ".$factor[1];
                    $disc= '<h3 class="product-disc extended">'.$discount.'</h3>';
                } else if($rel->promo_type == 'Free Stuff'){
                    $rel->potongan  = $rel->normal_price - $rel->promo_factor;
                    $rel->new_price = $rel->normal_price - $rel->potongan;
                    $discount="Free<br/>Stuff";
                    $disc= '<h3 class="product-disc extended">'.$discount.'</h3>';
                }

                ?>
            	<div class="col-3">
	                <a href="<?= base_url().$rel->sslug.'/product/'.$rel->id ?>">
						<div class="product">
							<div class="product-image">
								<img src="<?= base_url() ?>assets/uploads/<?= $rel->image ?>" alt="">
		                        <?= $disc ?>
							</div>
							<h3 class="product-name">
		                        <?= $rel->name ?>
							</h3>
							<!-- <p class="product-desc">
		                        <?= $rel->description ?>
							</p> -->
							<div class="product-detail">
								<div class="store-logo">
									<img src="<?= base_url() ?>assets/uploads/<?= $rel->simage ?>" alt="">
								</div>
								<div class="product-price">
									<h5 class="old-price">Rp. <?= number_format($rel->normal_price,0,',','.') ?>,-</h5>

									<h3 class="new-price">Rp. <?= number_format($rel->new_price,0,',','.') ?></h3>
								</div>
							</div>
						</div>
					</a>
				</div>
            <?php } ?>

			</div>
		</div>
	</div>
</div>
<?php } ?>
<script>
	// $('.wrapper-product').isotope({
	// 	masonry: {
	// 		gutter: 10
	// 	},
	// 	itemSelector: '.product',
	// 	percentPosition: true
	// });

	$("#kiri").stick_in_parent({
		offset_top: 120
	});
	$('input[name=display]').on('change', function() {
		var isi = $(this).val();
		if(isi == "list") {
			$('.result-gallery').addClass('list');
		} else {
			$('.result-gallery').removeClass('list');
		}
	})
</script>