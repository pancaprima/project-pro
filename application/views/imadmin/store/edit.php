<?=$map['js']?>
<div class='row'>
        <?php foreach($data as $d){ ?>
        <?= form_open_multipart(base_url()."imadmin/store/edit/".$d->id)?>
        <div class='col-1'>
            <div class='panel'>
                <div class='panel-body'>
                    <div class='input-row'>
                        <h5>Nama :</h5>
                        <input type='text' name="nama" value="<?php echo"$d->name" ?>"/>
                    </div>
                    <div class='input-row'>
                        <h5>Kategori :</h5>
                        <select name="kategori">
                                <option value="<?= $d->category->id; ?>"><?= $d->category->name; ?></option>
                                <?php 
                                    foreach($category as $c){
                                        echo "<option ";
                                        if($d->category_id==$c->id){
                                            echo "class='hidden' ";
                                        }
                                        echo "value=".$c->id.">".$c->name."</option>";
                                    }
                                ?>
                    
                        </select>   
                    </div>
                    <div class='input-row'>
                        <h5>Level :</h5>
                        <select name="level">
                        <option value='<?= $d->level; ?>'><?= $d->level; ?></option>
                            <option class='<?php if($d->level==0) echo "hidden";?>' value='0'>0</option>
                            <option class='<?php if($d->level==1) echo "hidden";?>' value='1'>1</option>
                            <option class='<?php if($d->level==2) echo "hidden";?>' value='2'>2</option>
                            <option class='<?php if($d->level==3) echo "hidden";?>' value='3'>3</option>
                        </select>
                        <!-- <p class="helper">Judul Postingan</p> -->
                    </div>
                    <div class='input-row'>
                        <h5>Alamat :</h5>
                        <input type='text' name='alamat' value="<?php echo"$d->address" ?>"/>
                        <!-- <p class="helper">Judul Postingan</p> -->
                    </div>
                    <div class='input-row file-exist'>
                        <h5>Featured Image :</h5>
                        <div class='input-file'>
                            <input type='text'/>
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name="image"/>
                        </div>
                        <div class='input-file-exist'>
                            <a href='<?= base_url()."assets/uploads/".$d->image; ?>' target='_blank' style='background-image:url(<?= base_url()."assets/uploads/".$d->image; ?>);'></a>
                            <h4><i class="fa fa-retweet"></i> Change Image</h4>
                        </div>
                        <p class="helper">Ukuran 2:1. Maks 2MB.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>
                    <div class='input-row'>
                        <h5>Deskripsi :</h5>
                        <textarea id='wysiwyg' name='deskripsi'><?= $d->description ?></textarea>
                    </div>
                    <script>
                        CKEDITOR.replace('wysiwyg');
                    </script>
                    <div class='input-row' style="height: 400px; overflow: hidden">
                          <h5>Lokasi :</h5>
                           <?=$map['html']?>
                    </div>
                    <div class='input-row submit'>
                        <input type='submit' value='Submit' class='button button-blue'/>
                        <input type='hidden' name='lat' id='lat' value='<?= $d->latitude ?>'>
                        <input type='hidden' name='long' id='long' value='<?= $d->longitude ?>'>
                        <input type='hidden' name="permalink" value='<?= $d->slug ?>' />
                    </div>
                </div>
            </div>
            
        </div>
        <?= form_close()?>
        <?php } ?>

</div>
<script type="text/javascript">
    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
    $("input[name='nama']").change(function(){
        var val = $(this).val();
        var slug = convertToSlug(val);
        $('input[name="permalink"]').val(slug);
    });
</script>