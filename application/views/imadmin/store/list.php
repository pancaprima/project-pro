<?= $this->session->flashdata("pesan") ?>
<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>Store</h5>
            </div>
            <div class='panel-body'>
                <table class='bordered table-blue datatable'>
                    <thead>
                        <tr>
							<th>Nama</th>
                            <th>Kategori</th>
                            <th>Level</th>
                            <th>Alamat</th>
							<th>Created At</th>
							<th>Updated At</th>
							
                            <th class='nosort'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $u) {
                        	echo "<tr>
                            <td>".$u->name."</td>
                            <td>".$u->category_id."</td>
                            <td>".$u->level."</td>
                            <td>".$u->address."</td>
							<td>".date("d M Y, H:i:s", strtotime($u->created))."</td>
                            <td>".date("d M Y, H:i:s", strtotime($u->updated))."</td>
                            <td class='nowrap'>
                                ".anchor(base_url()."imadmin/store/detail/$u->id", "<i class='fa fa-eye'></i> See Details", "class='button button-green'")." 
                                ".anchor(base_url()."imadmin/store/edit/$u->id", "<i class='fa fa-pencil'></i> Edit", "class='button button-yellow'")." 
                                ".anchor(base_url()."imadmin/store/delete/$u->id", "<i class='fa fa-trash-o'></i> Delete", "class='button button-red button-confirm'")."
                            </td>
                        </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>