<?= $map['js']; ?>
<div class='row'>

    <?php foreach($data as $d){ ?>
    <div class='col-3'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Nama :</h5>
                    <?= $d-> name ?>
                </div>
                
                <div class='input-row'>
                    <h5>Level :</h5>
                    <?= $d->level ?>
                </div>
                
                <div class='input-row'>
                    <h5>Alamat :</h5>
                    <p><?= $d->address ?></p>
                </div>

            </div>
        </div>
    </div>
    
    <div class='col-1'>
        <div class='panel' style='padding-top:2px'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Tanggal Dibuat :</h5>
                    <p> <?= $d->created ?></p>
                </div>

                <div class='input-row'>
                    <h5>Tanggal Diupdate :</h5>
                    <p> <?= $d->updated ?></p>
                </div>
            </div>
        </div>
    </div>
    
    <div class='col-3'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Featured Image :</h5>
                    <?php
                    if($d->image != ''){
                        echo"<img width='50%' align='middle' src='".base_url()."assets/uploads/".$d->image."' />"; ?> 
                    <?php
                    }else{
                        echo"Gambar Belum Ada";
                    }
                    ?>
                </div>

                <div class='input-row'>
                    <h5>Deskripsi :</h5>
                    <p> <?= $d->description ?></p>
                </div>
            </div>
        </div>
        <div class='map' style='height: 450px;'>
        <?= $map['html']; ?>
        </div>
    </div>
    <?php }?>

</div>