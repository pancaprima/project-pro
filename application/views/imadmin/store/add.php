<?= $this->session->flashdata('pesan') ?>
<?=$map['js']?>
<div class='row'>

        <?= form_open_multipart(base_url()."imadmin/store/add")?>
        <div class='col-1'>
            <div class='panel'>
                <div class='panel-body'>
                    <div class='input-row'>
                        <h5>Nama :</h5>
                        <input type='text' name="nama" />
                    </div>
                    <div class='input-row'>
                        <h5>Kategori :</h5>
                        <select name="kategori">
                                <?php
                                foreach($category as $c){
                                    echo "<option value=".$c->id.">".$c->name."</option>";
                                }
                                ?>
 
                        </select>   
                    </div>
                    <div class='input-row'>
                        <h5>Level :</h5>
                        <select name="level">
                            <option value='0'>0</option>
                            <option value='1'>1</option>
                            <option value='2'>2</option>
                            <option value='3'>3</option>
                        </select>
                        <!-- <p class="helper">Judul Postingan</p> -->
                    </div>
                    <div class='input-row'>
                        <h5>Alamat :</h5>
                        <input type='text' name='alamat' />
                        <!-- <p class="helper">Judul Postingan</p> -->
                    </div>
                    <div class='input-row'>
                        <h5>Featured Image :</h5>
                        <div class='input-file'>
                            <input type='text' />
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name='image'/>
                        </div>
                        <p class="helper">Ukuran gambar 2:1. Maks 2MB</p>
                    </div>
                </div>
            </div>
        </div>
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>
                    <div class='input-row'>
                        <h5>Deskripsi :</h5>
                        <textarea id='wysiwyg' name='deskripsi'></textarea>
                    </div>
                    <script>
                        CKEDITOR.replace('wysiwyg');
                    </script>
                    <div class='input-row' style='height: 340px'>
                          <h5>Lokasi :</h5>
                           <?=$map['html']?>
                    </div>
                    <div class='input-row submit'>
                        <input type='submit' value='Submit' class='button button-blue'/>
                        <input type='hidden' name='lat' id='lat'>
                        <input type='hidden' name='long' id='long'>
                        <input type="hidden" name="permalink" value='' />
                    </div>
                </div>
            </div>
            
        </div>

        <?= form_close()?>

</div>

<script type="text/javascript">
    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
    $("input[name='nama']").change(function(){
        var val = $(this).val();
        var slug = convertToSlug(val);
        $('input[name="permalink"]').val(slug);
    });
</script>