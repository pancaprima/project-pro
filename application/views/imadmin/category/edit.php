<div class='row'>
        
        <?= form_open_multipart(base_url()."imadmin/category/edit/".$data->id)?>
        <div class='col-4'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Nama Category :</h5>
                        <input type='text' name='name' value='<?= $data->name?>' /> 
                    </div>
                    
                    <div class='input-row file-exist'>
                        <h5>Featured Image :</h5>
                        <div class='input-file'>
                            <input type='text'/>
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name="image"/>
                        </div>
                        <div class='input-file-exist'>
                            <a href='<?= base_url()."assets/images/icon/elektronik.png" ?>' target='_blank' style='background-image:url(<?= base_url()."assets/images/icon/elektronik.png" ?>);'></a>
                            <h4><i class="fa fa-retweet"></i> Change Image</h4>
                        </div>
                        <p class="helper">Ukuran direkomendasikan 1:1. Maks 2MB</p>
                    </div>
                    <div class='input-row submit'>
                        <input type='submit' name="submitButton" value='Simpan' class='button button-blue'/>
                        
                    </div>
                </div>
            </div>
        </div>
        <?= form_close()?>

</div>
