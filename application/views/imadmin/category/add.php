<div class='row'>

        <?= form_open_multipart(base_url()."imadmin/category/add")?>
        <div class='col-1'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Nama Kategori:</h5>
                        <input type='text' name='name' /> 
                    </div>

                    
                    
                    <div class='input-row submit'>
                        <input type='submit' value='Tambah' class='button button-blue'/>
                    </div>
         
                </div>
            </div>
        </div>
        <?= form_close()?>
         
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>
                 <table class='bordered table-blue datatable'>
                        <thead>
                            <tr>
                                <th>Nama Kategori</th>
                                <th>Created</th>
                                <th>Updated</th>
                                
                                <th class='nosort'>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($data as $d) {
                                echo "<tr>
                                <td><a class='editableName' data-type='text' data-pk='".$d->id."' data-url='".base_url()."imadmin/category/edit' data-title='Enter name'>".$d->name."</a></td>
                                
                                <td>".date("d M Y, H:i:s", strtotime($d->created))."</td>
                                <td>".date("d M Y, H:i:s", strtotime($d->updated))."</td>
                                <td class='nowrap'> 
                                    ".anchor(base_url()."imadmin/category/edit/$d->id", "<i class='fa fa-pencil'></i> Edit", "class='button button-yellow'")."
                                </td>
                            </tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
</div>

<script type="text/javascript">
    $(document).ready( function () {
       $('.editableName').editable();
    }
</script>