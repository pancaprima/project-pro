<div class='row'>

    <?= form_open_multipart(base_url()."imadmin/slider/prosesAdd")?>
    <div class='col-1'>
        <div class='panel'>
            <div class='panel-body'>

                <div class='input-row'>
                    <h5>Gambar Utama :</h5>
                    <div class='input-file'>
                        <input type='text' />
                        <h4 class='button button-blue'>Browse</h4>
                        <input type="file" name='image' />
                    </div>
                    <p class="helper">Ukuran direkomendasikan 2:1 . Maks 2MB</p>
                </div>
                <div class='category-check'>
                        <h5>Category :</h5>
                        <input type="checkbox" name="category[]" value="Home" >Home</option>
                        <?php
                            foreach ($category as $category){
                                echo "<input type='checkbox' name='category[]' value='".$category->name."'>".$category->name."</option>";
                            }
                        ?>   
                </div>

                <div class='input-row submit'>
                    <input type='submit' value='Tambah' class='button button-blue'/>
                </div>

            </div>
        </div>
    </div>
    <?= form_close()?>

    <div class='col-3'>
        <div class='panel'>
            <div class='panel-body'>
                <table class='bordered table-blue datatable'>
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Category</th>
                        <th>Images</th>
                        <th>Created</th>
                        <th>Updated</th>

                        <th class='nosort'>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($data as $d) {
                        echo "<tr>                              
                                <td>".$d->status."</td>
                                <td>" .$d->page. "</td>
                                <td style='width:200px'> <img width='60%' src='".base_url()."/assets/uploads/".$d->image."'</td>
                                <td>".date("d M Y, H:i:s", strtotime($d->created))."</td>
                                <td>".date("d M Y, H:i:s", strtotime($d->updated))."</td>
                                <td class='nowrap'>";
                                    if ($d->status == 'active') {
                                       echo "<a href='".base_url()."imadmin/slider/status/".$d->id."/non-active' class='button button-yellow'><i class='fa fa-close'></i> Non-Actived</a>";
                                    } else {
                                        echo "<a href='".base_url()."imadmin/slider/status/".$d->id."/active' class='button button-green'><i class='fa fa-check'></i> Actived</a>";
                                    }
                                    echo anchor(base_url()."imadmin/slider/delete/$d->id", "<i class='fa fa-trash-o'></i> Delete", "class='button button-red button-confirm' style='margin-left:4px'")."
                                </td>
                            </tr>";
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready( function () {
        $('.editableName').editable();
    }
</script>