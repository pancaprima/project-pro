<?= $this->session->flashdata("pesan") ?>
<div class='row'>
    <div class='col-4'>
        <div class='panel'>
            <div class='panel-head'>
                <h5>Product</h5>
            </div>
            <div class='panel-body'>
                <table class='bordered table-blue datatable'>
                    <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Tipe Promo</th>
                            <th>Harga Normal</th>
                            <th>Faktor Promo</th>
                            <th>Diskon</th>
                            <th>Tag</th>
                            <th>Created</th>
                            <th>Update</th>
                            
                            <th class='nosort'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $p) {
                            echo "<tr>
                            <td>".$p->name."</td>
                            <td>".$p->promo_type."</td>
                            <td>".$p->normal_price."</td>
                            <td>".$p->promo_factor."</td>
                            <td>".$p->discount."</td>
                            <td>".$p->tag."</td>
                            <td>".date("d M Y, H:i:s", strtotime($p->created))."</td>
                            <td>".date("d M Y, H:i:s", strtotime($p->updated))."</td>
                            <td class='nowrap'>
                                ".anchor(base_url()."imadmin/product/detail/$p->id", "<i class='fa fa-eye'></i> See Details", "class='button button-green'")." 
                                ".anchor(base_url()."imadmin/product/edit/$p->id", "<i class='fa fa-pencil'></i> Edit", "class='button button-yellow'")."
                                ".anchor(base_url()."imadmin/product/delete/$p->id", "<i class='fa fa-trash-o'></i> Delete", "class='button button-red button-confirm'")."
                            </td>
                        </tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>