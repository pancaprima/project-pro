<div class='row'>
        <?php foreach($data as $d){ ?>
        <?= form_open_multipart(base_url()."imadmin/product/edit/".$d->id)?>
        <div class='col-1'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Harga Normal :</h5>
                        <input type='text' name='harga_normal' value='<?= $d->normal_price ?>' /> 
                    </div>
                    
                    <div class='input-row'>
                        <h5>Tipe Promo:</h5>
                        <select id="promo_type" name="promo_type">
                            <option value="<?= $d->promo_type; ?>"><?= $d->promo_type; ?></option>
                            <option class='<?php if($d->promo_type=="Discount") echo 'hidden'; ?>' value="Discount">Discount</option>
                            <option class='<?php if($d->promo_type=="Buy n Get n") echo 'hidden'; ?>' value="Buy n Get n">Buy n Get n</option>
                            <option class='<?php if($d->promo_type=="Free Stuff") echo 'hidden'; ?>' value="Free Stuff">Free Stuff</option>
                            <option class='<?php if($d->promo_type=="New Price") echo 'hidden'; ?>' value="New Price">New Price</option>
                        </select>   
                    </div>

                    <div class='input-row'>
                        <div id='discount_promo' class='input-row'>
                            <h5>Besar Diskon:</h5>
                            <input type='range' id='discount_range' class='range' value="<?= $d->discount; ?>" name='discount' min='5' max'100' step='5'/>
                            <h4 id='discount_value'><?= $d->discount; ?>%</h4>
                        </div>
                    </div>
                    <div id='buy-n_promo' class='hidden'>
                        <div class='input-row inline'>
                            <?php
                               if($d->promo_type=="Buy n Get n"){
                                   $explode= explode(",",$d->promo_factor);
                                   $buy = $explode[0];
                                   $get = $explode[1];
                               }else{ $buy=0; $get=0;}
                             ?>
                            <h5>Buy :</h5> <input type='text' name='buy' value="<?= $buy; ?>"/>
                             
                        </div>
                        <div class='input-row inline'>
                            <h5>Get :</h5> <input type='text' name='get' value="<?= $get; ?>"/> 
                        </div>
                    </div>
                    
                    <div class='input-row hidden' id='price_promo'>
                        <h5>Harga Promo :</h5>
                        <input type='text' name='harga_promo' value='<?php if($d->promo_type=="New Price") echo $d->promo_factor; ?>' /> 
                    </div>
                    
                    <div class="input-row hidden" id="freestuff_promo">
                        <h5>Harga Free Stuff :</h5>
                        <input type="text" name="harga_freestuff" value='<?php if($d->promo_type=="Free Stuff") echo $d->promo_factor; ?>' />
                    </div>

                    <div class='input-row'>
                        <h5>Tag :</h5>
                        <input type='text' name="tag" value='<?= $d->tag; ?>'/>
                    </div>

                </div>
            </div>
        </div>
        <input type="hidden" name="permalink" />
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Nama Produk:</h5>
                        <input type="text" name="nama_produk" value='<?= $d->name; ?>'>   
                    </div>
                    <div class='input-row file-exist'>
                        <h5>Featured Image :</h5>
                        <div class='input-file'>
                            <input type='text'/>
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name="image"/>
                        </div>
                        <div class='input-file-exist'>
                            <a href='<?= base_url()."assets/uploads/".$d->image; ?>' target='_blank' style='background-image:url(<?= base_url()."assets/uploads/".$d->image; ?>);'></a>
                            <h4><i class="fa fa-retweet"></i> Change Image</h4>
                        </div>
                        <p class="helper">Ukuran direkomendasikan 1:1. Maks 2MB</p>
                    </div>

                    <div class='input-row'>
                        <h5>Deskripsi :</h5>
                        <textarea id='wysiwyg' name='description'><?= $d->description ?></textarea>
                    </div>
                    <script>
                        CKEDITOR.replace('wysiwyg');
                    </script>
                    <div class='input-row submit'>
                        <input type='submit' name="submitButton" value='Simpan' class='button button-blue'/>
                        <input type="hidden" name="id_promo" value="<?= $d->id_promo; ?>">
                    </div>
                </div>
            </div>
        </div>
        <?= form_close()?>
        <?php } ?>

</div>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#promo_type').on('change', function() {
            var promo_type_val=$(this).val();
            
            $("#discount_promo").addClass("hidden");
            $("#buy-n_promo").addClass("hidden");
            $("#price_promo").addClass("hidden");
            $("#freestuff_promo").addClass("hidden");
            console.log(promo_type_val);

            switch(promo_type_val){
                case "Buy n Get n"  : $("#buy-n_promo").removeClass("hidden"); break;
                case "New Price"    : $("#price_promo").removeClass("hidden"); break;
                case "Discount"     : $("#discount_promo").removeClass("hidden"); break;
                case "Free Stuff"   : $("#freestuff_promo").removeClass("hidden"); break;

                default: break;
            }   
        }).change();

        $('#discount_range').on('change', function() {
            var discount_val=$(this).val();
            $( "#discount_value" ).empty();
            $( "#discount_value" ).append( discount_val+" %" );
        });

    });
</script>