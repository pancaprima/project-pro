<?= $this->session->flashdata('pesan') ?>

<script>
    function getValidation(){
        var buy = $('#buy').val();
        var get = $('#get').val();

        if (buy >= get) {
            alert('input get harus lebih besar');
        };
    }
</script>

<div class='row'>

        <?= form_open_multipart(base_url()."imadmin/product/add")?>
        <div class='col-1'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Promo :</h5>
                        <select name="id_promo">
                            <?php
                            foreach ($data as $p) {
                                if(substr($p->id,0,2) != "PO") echo "<option value=".$p->id.">".$p->title."</option>";
                            }
                            ?>
                        </select>
                    </div>

                    <div class='input-row'>
                        <h5>Harga Normal :</h5>
                        <input type='text' name='harga_normal' /> 
                    </div>
                    
                    <div class='input-row'>
                        <h5>Tipe Promo:</h5>
                        <select id="promo_type" name="promo_type">
                            <option value="Discount">Discount</option>
                            <option value="Buy n Get n">Buy n Get n</option>
                            <option value="Free Stuff">Free Stuff</option>
                            <option value="New Price">New Price</option>
                        </select>   
                    </div>

                    <div class='input-row'>
                        <div id='discount_promo' class='input-row'>
                            <h5>Besar Diskon:</h5>
                            <input type='range' id='discount_range' class='range' value="50" name='discount' min='5' max'100' step='5'/>
                            <h4 id='discount_value'>50%</h4>
                        </div>
                    </div>
                    <div id='buy-n_promo' class='hidden'>
                        <div class='input-row inline'>
                            <h5>Buy :</h5> <input id="buy" type='text' name='buy' />
                             
                        </div>
                        <div class='input-row inline'>
                            <h5>Get :</h5> <input id="get" type='text' name='get' onChange='getValidation()'/> 
                        </div>
                    </div>
                    
                    <div class='input-row hidden' id='price_promo'>
                        <h5>Harga Promo :</h5>
                        <input type='text' name='harga_promo' /> 
                    </div>
                    
                    <div class="input-row hidden" id="freestuff_promo">
                        <h5>Harga Free Stuff :</h5>
                        <input type="text" name="harga_freestuff">
                    </div>

                    <div class='input-row'>
                        <h5>Tag :</h5>
                        <input type='text' name="tag" />
                    </div>

                </div>
            </div>
        </div>
        <input type="hidden" name="permalink" />
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Nama Produk:</h5>
                        <input type="text" name="nama_produk">   
                    </div>

                    <div class='input-row'>
                        <h5>Gambar Utama :</h5>
                        <div class='input-file'>
                            <input type='text' />
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name='image'/>
                        </div>
                        <p class="helper">Ukuran direkomendasikan 1:1 . Maks 2MB</p>
                    </div>

                    <div class='input-row'>
                        <h5>Deskripsi :</h5>
                        <textarea id='wysiwyg' name='description'></textarea>
                    </div>
                    <script>
                        CKEDITOR.replace('wysiwyg');
                    </script>
                    <div class='input-row submit'>
                        <input type='submit' name="submitButton" value='Simpan dan Tambah Produk Lain' class='button button-green'/>
                        <input type='submit' name="submitButton" value='Simpan' class='button button-blue'/>
                    </div>
                </div>
            </div>
        </div>
        <?= form_close()?>

</div>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#promo_type').on('change', function() {
            var promo_type_val=$(this).val();
            
            $("#discount_promo").addClass("hidden");
            $("#buy-n_promo").addClass("hidden");
            $("#price_promo").addClass("hidden");
            $("#freestuff_promo").addClass("hidden");
            console.log(promo_type_val);

            switch(promo_type_val){
                case "Buy n Get n"  : $("#buy-n_promo").removeClass("hidden"); break;
                case "New Price"    : $("#price_promo").removeClass("hidden"); break;
                case "Discount"     : $("#discount_promo").removeClass("hidden"); break;
                case "Free Stuff"   : $("#freestuff_promo").removeClass("hidden"); break;

                default: break;
            }   
        });

        $('#discount_range').on('change', function() {
            var discount_val=$(this).val();
            $( "#discount_value" ).empty();
            $( "#discount_value" ).append( discount_val+" %" );
        });

    });
</script>