<div class='row'>

    <?php foreach($data as $d){ ?>
    <div class='col-2'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Nama Produk :</h5>
                    <?= $d-> name ?>
                </div>
                
                <div class='input-row'>
                    <h5>Tipe Promo :</h5>
                    <?= $d-> promo_type ?>
                </div>
                
                <div class='input-row'>
                    <h5>Harga Normal :</h5>
                    <?= $d-> normal_price ?>
                </div>
				
				<div class='input-row'>
                    <h5>Faktor Promo :</h5>
                    <?= $d-> promo_factor ?>
                </div>
				
				<div class='input-row'>
                    <h5>Diskon :</h5>
                    <?= $d-> discount ?>%
                </div>
				
				<div class='input-row'>
                    <h5>Tag :</h5>
                    <?= $d-> tag ?>
                </div>

            </div>
        </div>
    </div>
    
    <div class='col-1'>
        <div class='panel' style='padding-top:2px'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Tanggal Dibuat :</h5>
                    <p> <?= $d->created ?></p>
                </div>

                <div class='input-row'>
                    <h5>Tanggal Diupdate :</h5>
                    <p> <?= $d->updated ?></p>
                </div>
            </div>
        </div>
    </div>
    
    <div class='col-3'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Featured Image :</h5>
                    <?php
                    if($d->image != ''){
                        echo"<img width='50%' align='middle' src='".base_url()."assets/uploads/".$d->image."' />"; ?> 
                    <?php
                    }else{
                        echo"Gambar Belum Ada";
                    }
                    ?> 
                </div>

                <div class='input-row'>
                    <h5>Deskripsi :</h5>
                    <p> <?= $d->description ?></p>
                </div>
            </div>
        </div>

    </div>
    <?php }?>

</div>