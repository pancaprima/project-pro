<div class='row'>
    
    <?php foreach($data as $d){ ?>
    <?= form_open_multipart(base_url()."imadmin/promo/edit/".$d->id)?>
    <div class='col-1'>
        <div class='panel'>
            <div class='panel-body'>
                <div class='input-row'>
                    <h5>Tanggal Mulai :</h5>
                    <input type='text 'name='start_date' id='start_date' value='<?= $d->start_date; ?>'/> 
                </div>
                <div class='input-row'>
                    <h5>Tanggal Berakhir :</h5>
                    <input type='text' name='end_date' id='end_date' value='<?= $d->end_date; ?>'/> 
                </div>
                
                <div class='input-row'>
                    <h5>Promo untuk semua item pada toko?</h5>
                    <select id='items' name="items">
                        <?php 
                        $allItemPromo = substr($d->id,0,2);
                        if($allItemPromo == "PO") {echo "<option value='y'>Semua item</option>";}
                        else { echo "<option value='n'>Beberapa item</option>";}
                        ?>         
                    </select>   
                </div>
                <div id='promo_detail' class='<?php if($allItemPromo != "PO") {echo "hidden";}  ?>'>
                    <div class='input-row'>
                        <h5>Tipe Promo:</h5>
                        <select id="promo_type" name="promo_type">
                            <?= "<option value='".$d->promo_type."'>".$d->promo_type."</option>"; ?>
                            <option class='<?php if($d->promo_type=="Discount") echo "hidden";?>' value="Discount">Discount</option>
                            <option class='<?php if($d->promo_type=="Buy n Get n") echo "hidden";?>' value="Buy n Get n">Buy n Get n</option>
                        </select>   
                    </div>

                    <div class='input-row'>
                        <div id='discount_promo' class='input-row'>
                            <h5>Besar Diskon:</h5>
                            <input type='range' id='discount_range' class='range' value="<?= $d->discount ?>" name='discount' min='5' max'100' step='5'/>
                            <h4 id='discount_value'><?= $d->discount ?>%</h4>
                        </div>
                    </div>
                    <div id='buy-n_promo' class='hidden'>
                        <div class='input-row inline'>
                            <?php
                            $get=0;
                            if($d->promo_type =="Buy n Get n"){
                                $explode = explode(".",$d->discount);
                                $buy     = $explode[1];
                                $get     = round($buy*100/$explode[0]);
                            } 
                            ?>
                            <h5>Buy :</h5> <input type='text' name='buy' value='<?= $buy; ?>'/>
                        </div>
                        <div class='input-row inline'>
                            <h5>Get :</h5> <input type='text' name='get' value='<?= $get; ?>' /> 
                        </div> 
                    </div>
                </div> 
            </div>
        </div>
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>
                    <div class='input-row'>
                        <h5>Judul :</h5>
                        <input type='text' name="title" value='<?= $d->title; ?>' />
                    </div>
                    <div class='input-row file-exist'>
                        <h5>Featured Image :</h5>
                        <div class='input-file'>
                            <input type='text'/>
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name="image"/>
                        </div>
                        <div class='input-file-exist'>
                            <a href='<?= base_url()."assets/uploads/".$d->image; ?>' target='_blank' style='background-image:url(<?= base_url()."assets/uploads/".$d->image; ?>);'></a>
                            <h4><i class="fa fa-retweet"></i> Change Image</h4>
                        </div>
                        <p class="helper">Ukuran gambar 1:1. Maks 2MB</p>
                    </div>
                    <div class='input-row'>
                        <h5>Deskripsi :</h5>
                        <textarea id='wysiwyg' name='description'><?= $d->description; ?></textarea>
                    </div>
                    <script>
                        CKEDITOR.replace('wysiwyg');
                    </script>
                    <div class='input-row submit'>
                        <input type='submit' name='button-simpan' value='Simpan' class='button button-green' />
                        <input type='hidden' name="permalink" value='<?= $d->slug ?>' />
                    </div>

                </div>
            </div>
        </div>
        <?= form_close()?>
        <?php } ?>
</div>

<script type="text/javascript">
    
    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
    $("input[name='nama']").change(function(){
        var val = $(this).val();
        var slug = convertToSlug(val);
        $('input[name="permalink"]').val(slug);
    });
    
    $(document).ready(function(){
         $('#items').on('change', function() {
             var items_val=$(this).val();
             
             switch(items_val){
                case "n"    : $("#promo_detail").addClass("hidden"); $("#submit_add").removeClass("hidden");break;
                case "y"    : $("#promo_detail").removeClass("hidden"); $("#submit_add").addClass("hidden");break;

                default: break; 
             }
             
             
         });
         
         $('#promo_type').on('change', function() {
            var promo_type_val=$(this).val();
            
            $("#discount_promo").addClass("hidden");
            $("#buy-n_promo").addClass("hidden");

            switch(promo_type_val){
                case "Buy n Get n"  : $("#buy-n_promo").removeClass("hidden"); break;
                case "Discount"     : $("#discount_promo").removeClass("hidden"); break;

                default: break;
            }   
        }).change();

        $('#discount_range').on('change', function() {
            var discount_val=$(this).val();
            $( "#discount_value" ).empty();
            $( "#discount_value" ).append( discount_val+" %" );
        });
            

        $('#start_date').datetimepicker({
           format: "Y-m-d H:i",
           minDate: 0,
           minTime: 0,
           onShow:function( ct ){
                  var datetime=$('#end_date').val();
                  if(datetime != '') {
                        var Y= datetime.substring(0,4);
                        var m= datetime.substring(5,7);
                        var d= datetime.substring(8,10);
                        var date= Y+"/"+m+"/"+d;
                        console.log(date);
                        this.setOptions({
                            maxDate:date,
                        });
                  }  
                }
        });
        
        $('#end_date').datetimepicker({
           format: "Y-m-d H:i",
           onShow:function( ct ){
                  var datetime=$('#start_date').val();
                  if(datetime != '') {
                        var Y= datetime.substring(0,4);
                        var m= datetime.substring(5,7);
                        var d= datetime.substring(8,10);
                        var date= Y+"/"+m+"/"+d;
                        console.log(date);
                        this.setOptions({
                            minDate:date,
                        });
                  }  
                }
        });
        
    });
</script>

