<?= $this->session->flashdata('pesan') ?>
<?= $this->session->flashdata('pesan2') ?>

<script>
    $('document').ready(function(){
       $('.imgKedua').hide();
    });
    function checkLevel(){
        var id  = $("#store").val();
        var level = $('.str[data-id="'+id+'"]').data('level');
        if(level == '2' || level =='4'){
            $('.imgKedua').show();
            $("#lvl").val(level);
        }else{
            $('.imgKedua').hide();
            $("#lvl").val(level);
        }
    }
</script>
<div class='row'>
        <?php echo validation_errors(); ?>
        <?= form_open_multipart(base_url()."imadmin/promo/add")?>
        <div class='col-1'>
            <div class='panel'>
                <div class='panel-body'>
                    <div class='input-row'>
                        <h5>Store :</h5>
                        <select name="id_store" id="store" onchange="checkLevel()">
                            <option selected disabled>Select Store</option>
                        <?php
                            foreach ($data as $store){
                                echo "<option value='".$store->id."' class='str' data-id='".$store->id."' data-level='".$store->level."'>".$store->name."</option>";
                            }
                        ?>
                        </select>
                        <input type="hidden" id="lvl"/>
                    </div>
                    
                    <div class='input-row'>
                        <h5>Tanggal Mulai :</h5>
                        <input type='text 'name='start_date' id='start_date'/> 
                    </div>
                    <div class='input-row'>
                        <h5>Tanggal Berkahir :</h5>
                        <input type='text' name='end_date' id='end_date'/> 
                    </div>
                    
                    <div class='input-row'>
                        <h5>Promo untuk semua item ?</h5>
                        <select id='items' name="items">
                                <option value="n">Tidak</option>
                                <option value="y">Semua</option>
                        </select>   
                    </div>
                    <div id='promo_detail' class='hidden'>
                        <div class='input-row'>
                            <h5>Tipe Promo:</h5>
                            <select id="promo_type" name="promo_type">
                                <option value="Discount">Discount</option>
                                <option value="Buy n Get n">Buy n Get n</option>
                            </select>   
                        </div>
    
                        <div class='input-row'>
                            <div id='discount_promo' class='input-row'>
                                <h5>Besar Diskon:</h5>
                                <input type='range' id='discount_range' class='range' value="50" name='discount' min='5' max'100' step='5'/>
                                <h4 id='discount_value'>50%</h4>
                            </div>
                        </div>
                        <div id='buy-n_promo' class='hidden'>
                            <div class='input-row inline'>
                                <h5>Buy :</h5> <input type='text' name='buy' />
                                 
                            </div>
                            <div class='input-row inline'>
                                <h5>Get :</h5> <input type='text' name='get' /> 
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
        <div class='col-3'>
            <div class='panel'>
                <div class='panel-body'>

                    <div class='input-row'>
                        <h5>Judul :</h5>
                        <input type='text' name="title" />
                    </div>

                    <div class='input-row'>
                        <h5>Featured Image :</h5>
                        <div class='input-file'>
                            <input type='text' />
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name='image[]'/>
                        </div>
                        <p class="helper">Ukuran gambar 1:1. Maks 2MB</p>
                    </div>

                    <div class='input-row imgKedua' >
                        <h5>Image 2x1:</h5>
                        <div class='input-file'>
                            <input type='text' />
                            <h4 class='button button-blue'>Browse</h4>
                            <input type="file" name='image[]'/>
                        </div>
                        <p class="helper">Ukuran gambar 2:1. Maks 2MB</p>
                    </div>

                    <div class='input-row'>
                        <h5>Deskripsi :</h5>
                        <textarea id='wysiwyg' name='description'></textarea>
                    </div>
                    <script>
                        CKEDITOR.replace('wysiwyg');
                    </script>
                    <div class='input-row submit'>
                        <input id='submit_only' type='submit' name='button-simpan' value='Simpan' class='button button-blue hidden' />
                        <input id='submit_add' type='submit' name='button-simpan' value='Simpan dan Tambah Produk' class='button button-blue' />
                        <input type="hidden" name="permalink" value='' />
                    </div>

                </div>
            </div>
        </div>
        <?= form_close()?>

</div>
<script type="text/javascript">
    function convertToSlug(Text) {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            .substring(0,25)
            ;
    }

    $("input[name='title']").change(function(){
        var val = $(this).val();
        var slug = convertToSlug(val);
        $('input[name="permalink"]').val(slug);
    });

    $(document).ready(function(){
         $('#items').on('change', function() {
             var items_val=$(this).val();
             $("#submit_add").addClass("hidden");
             $("#submit_only").addClass("hidden");
             
             
             switch(items_val){
                case "n"    : $("#promo_detail").addClass("hidden"); $("#submit_add").removeClass("hidden");break;
                case "y"    : $("#promo_detail").removeClass("hidden"); $("#submit_only").removeClass("hidden");break;

                default: break; 
             }
             
             
         });
         
         $('#promo_type').on('change', function() {
            var promo_type_val=$(this).val();
            
            $("#discount_promo").addClass("hidden");
            $("#buy-n_promo").addClass("hidden");

            switch(promo_type_val){
                case "Buy n Get n"  : $("#buy-n_promo").removeClass("hidden"); break;
                case "Discount"     : $("#discount_promo").removeClass("hidden"); break;

                default: break;
            }   
        });

        $('#discount_range').on('change', function() {
            var discount_val=$(this).val();
            $( "#discount_value" ).empty();
            $( "#discount_value" ).append( discount_val+" %" );
        });
            

        $('#start_date').datetimepicker({
           format: "Y-m-d H:i",
           minDate: 0,
           onShow:function( ct ){
                  var datetime=$('#end_date').val();
                  if(datetime != '') {
                        var Y= datetime.substring(0,4);
                        var m= datetime.substring(5,7);
                        var d= datetime.substring(8,10);
                        var date= Y+"/"+m+"/"+d;
                        console.log(date);
                        this.setOptions({
                            maxDate:date
                        });
                  }  
                }
        });
        
        $('#end_date').datetimepicker({
           format: "Y-m-d H:i",
           onShow:function( ct ){
                  var datetime=$('#start_date').val();
                  if(datetime != '') {
                        var Y= datetime.substring(0,4);
                        var m= datetime.substring(5,7);
                        var d= datetime.substring(8,10);
                        var date= Y+"/"+m+"/"+d;
                        console.log(date);
                        this.setOptions({
                            minDate:date
                        });
                  }  
                }
        });
        
    });
</script>

