
<aside>
            <div class='profil'>
                <div class='foto' style='background-image:url(<?=base_url()?>assets/images/pp.jpg);'>
                </div>
                <div class='info'>
                    <h4><?= $this->session->userdata('loggedin')['name'] ?></h4>
                </div>
                <div class='opsi'>
                    <div class='button-group'>
                        <i class="fa fa-cog dropdown-toggle"></i>
                        <ul class='dropdown-menu right-pos'>
                            <li class='head'>Profile Option</li>
                            <li><a href='#'>Profile</a></li>
                            <li><a href='#'>Settings</a></li>
                            <li class='sparator'></li>
                            <li><a href='<?= base_url() ?>imadmin/login/logout'>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class='nav outer'>
                <li <?= $pos_parent == 'dashboard' ? "class='active'" : ""; ?>><a href='<?= base_url() ?>imadmin/'><i class="fa fa-tachometer"></i>Dashboard</a></li>

                <li class='second <?= $pos_parent == 'store' ? "active" : ""; ?>'><a><i class="fa fa-shopping-cart"></i>Store<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'store' && $pos_child == 'list' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/store/'>List</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'store' && $pos_child == 'add' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/store/add'>Add</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'store' && $pos_child == 'category' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/category'>Category Management</a></li>
                    </ul>
                </li>

                <li class='second <?= $pos_parent == 'promo' ? "active" : ""; ?>'><a><i class="fa fa-tags"></i>Promo<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'promo' && $pos_child == 'list' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/promo/'>List</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'promo' && $pos_child == 'add' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/promo/add'>Add</a></li>
                    </ul>
                </li>

                <li class='second <?= $pos_parent == 'product' ? "active" : ""; ?>'><a><i class="fa fa-suitcase"></i>Product<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'product' && $pos_child == 'list' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/product/'>List</a></li>
                        <li <?= isset($pos_child) && $pos_parent == 'product' && $pos_child == 'add' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/product/add'>Add</a></li>
                    </ul>
                </li>

                <li class='second <?= $pos_parent == 'slider' ? "active" : ""; ?>'><a><i class="fa fa-sliders"></i>Slider<i class='fa arrow'></i></a>
                    <ul class='nav-second'>
                        <li <?= isset($pos_child) && $pos_parent == 'slider' && $pos_child == 'add' ? "class='active'" : ""; ?>>
                            <a href='<?= base_url() ?>imadmin/slider/add'>Add</a></li>
                    </ul>
                </li>
                
            </ul>
        </aside>