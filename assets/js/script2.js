
$(document).ready(function() {
  
    //DATATABLES
    $('.datatable').DataTable({
        'aoColumnDefs': [{
            'bSortable': false,
            'aTargets': ['nosort']
        }]
    });
    $('.datatable-nosort').DataTable({
        ordering: false
    });
	
});